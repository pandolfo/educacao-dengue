PERGUNTAS	ALTERNATIVA A	RESPOSTA	IMAGEM	LIVRO	COUNT
Posso deixar meus ovinhos em qualquer lugar com a água parada.	Não	Sim	question_img_01	Livro azul - pag 22	1
O vírus é transmitido apenas pela picada do mosquito infectado.	Não	Sim	question_img_02	Livro laranja - Pág 12	2
Mosquito sem vírus não transmite doença	Não	Sim	question_img_03	Livro laranja - Pág 12	3
Aqui, existem locais propícios para a  reprodução do mosquito?	Não	Sim	question_img_04	Livro laranja - Pág 13	4
Todo o mosquito transmite a Dengue	Sim	Não	question_img_05	Livro laranja - Pág 14	5
A pessoa infectada pode transmitir a Dengue pelo espirro.	Não	Sim	question_img_06	Livro laranja - Pág 15	6
Estas garrafas estão armazenadas de maneira correta?	Não	Sim	question_img_07	Livro laranja - Pág 16  	7
O mosquito Aedes aegypti transmite também a febre chikungunya, o zika vírus e a febre amarela urbana.	Não	Sim	question_img_08	Livro azul - Pág 6 	8
O mosquito Aedes aegypti é perigoso 	Não	Sim	question_img_09	Livro laranja - Pág 16	9
A dengue é uma doença infecciosa provocada por um vírus chamado DENV.	Não	Sim	question_img_10	Livro azul - Pág 12	10
Se você for picado por um mosquito sem o vírus você também também fica doente.	Sim	Não	question_img_11	Livro azul - Pág 18	11
O vírus se desenvolve no organismo do inseto, mas não o deixa doente como acontece com o ser humano.	Não	Sim	question_img_12	Livro azul - Pág 18	12
Se o ovo do mosquito ficar foda da água ele morre emminutos. 	Sim	Não	question_img_13	Livro azul - Pág 25	13
Para eliminar os ovos é necessário lavar os recipientes com água e sabão.	Não	Sim	question_img_14	Livro azul - Pág 26	14
O mosquito Aedes aegypti vive cerca de 45 dias.	Não	Sim	question_img_15	Livro azul - Pág 29	15
Toda e qualquer medicação deve ser usada sempre sob orientação médica.	Não	Sim	question_img_16	Livro azul - Pág 33	16
A hidratação é fundamental para o tratamento dos sintomas da dengue. 	Não	Sim	question_img_17	Livro azul - Pág 35	17
O combate ao Aedes aegypti necessita da colaboração de todos	Não	Sim	question_img_18	Livro azul - Pág 37	18
A vacinação é uma das formas mais seguras para cuidarmos de nossa saúde.	Não	Sim	question_img_19	Livro azul - Pág 43	19
O Coronavírus tem esse nome porque seu formato lembra uma coroa (em latim, “corona”) quando observado em microscópio.	Não	Sim	question_img_20	Livro azul - Pág 44	20
A falta de higiene tem sido uma das principais causas do surgimento de surtos de doenças em todas as épocas.	Não	Sim	question_img_21	Livro azul - Pág 47	21
O hábito de higienizar as mãos com frequência está entre os meios mais eficazes para evitar a sua transmissão.	Não	Sim	question_img_22	Livro azul - Pág 47	22
Quem pega Dengue precisa ir ao médico.	Não	Sim	question_img_23	Livro Laranja - Pág 8	23
Quando eu perceber uma vasilha com água, devo tampa-la.	Não	Sim	question_img_24	Livro Laranja - Pág 18	24
Mesmo coberto e longe da água, os pneus podem servir de criadouro do mosquito.	Sim	Não	question_img_25	Livro Laranja - Pág 23	25
As vacinas são muito importantes. Tomar vacina é cuidar da saúde!	Não	Sim	question_img_26	Livro Laranja - Pág 26	26
A Dengue é um problema social, por isso, cada pessoa precisa aprender e fazer a sua parte.	Não	Sim	question_img_27	Livro Roxo - Pág 32	27
Ser um Agente Protetor é ensinar as pessoas a importância do combate ao mosquito e fiscalizar.	Não	Sim	question_img_28	Livro Roxo - Pág 36	28
O trabalho de um Agente Protetor começa dentro da sua casa. Ensine seus pais sobre a prevenção do mosquito e elimine os focos de Dengue que encontrar.	Não	Sim	question_img_29	Livro Roxo - Pág 37	29
Comer alho e cebola afastam os mosquitos	Sim	Não	question_img_30	Livro Roxo - Pág 39	30
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ClearCacheEditor : Editor
{
    [MenuItem("Tools/Clear PlayerPrefs")]
    public static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}

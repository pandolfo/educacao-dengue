using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Challenges")]
public class ChallengesData : ScriptableObject
{
    [SerializeField] private List<ChallengeData> _challenges;
    [SerializeField] private List<ChallengeData> _fixedChallenges;

    public int ChallengesCount => _challenges.Count;
    public int TotalChallengesCount => _challenges.Count + _fixedChallenges.Count;

    public List<ChallengeData> Challenges => _challenges;
    public List<ChallengeData> FixedChallenges => _fixedChallenges;
    public List<ChallengeData> TotalChallenges
    {
        get
        {
            List<ChallengeData> challenges = new List<ChallengeData>();
            challenges.AddRange(_challenges);
            challenges.AddRange(_fixedChallenges);
            return challenges;
        }
    }
}

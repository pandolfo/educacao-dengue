﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHelper : MonoBehaviour
{
    public void PlaySFX(AudioClip clip)
    {
        AudioManager.PlaySFX(clip);
    }

    public void PlayVoice(string voice)
    {
        AudioManager.PlayVoice(voice);
    }
}

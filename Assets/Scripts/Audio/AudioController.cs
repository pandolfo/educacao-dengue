﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class GameAudio
{
    public string Name;
    public AudioClip Clip;
}

[System.Serializable]
public class SFX
{
    public AudioClip Clip;
    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
            {
                _audioSource = new GameObject($"{Clip.name} Audio").AddComponent<AudioSource>();
                _audioSource.clip = Clip;
                _audioSource.spatialBlend = 0;
                _audioSource.playOnAwake = false;
                _audioSource.transform.SetParent(AudioManager.Instance.transform);
                _audioSource.outputAudioMixerGroup = AudioManager.Instance.AudioController.AudioMixer.FindMatchingGroups("SFX")[0];
            }

            return _audioSource;
        }
    }

    public SFX(AudioClip clip)
    {
        Clip = clip;
    }
}

[System.Serializable]
public class SFXList
{
    public AudioClip ButtonClicked;
    public AudioClip Correct;
    public AudioClip Error;
    public AudioClip Victory;
    public AudioClip Defeat;
}

[System.Serializable]
public class MusicList
{
    public AudioClip MainMenu;
    public AudioClip WordHunting;
    public AudioClip MosquitoHunting;
    public AudioClip Characteristics;
    public AudioClip Quiz;
    public AudioClip Cycles;
}

[System.Serializable]
public class VoiceList
{
    public List<AudioClip> VoiceClips;

    public AudioClip GetVoiceClip(string name)
    {
        AudioClip voice = VoiceClips.Find(c => c.name == name);

        if (voice == null)
            voice = Resources.Load<AudioClip>($"Voice/{name}");

        return voice;
    }
}

[CreateAssetMenu]
public class AudioController : ScriptableObject
{
    [SerializeField] private AudioMixer _audioMixer;

    [SerializeField] private SFXList _sfx;
    [SerializeField] private MusicList _music;
    [SerializeField] private VoiceList _voice;

    public AudioMixer AudioMixer => _audioMixer;

    public SFXList SFXList => _sfx;

    public MusicList MusicList => _music;

    public VoiceList VoiceList => _voice;
}

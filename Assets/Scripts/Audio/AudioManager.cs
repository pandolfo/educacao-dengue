using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    private List<SFX> _sfxs = new List<SFX>();

    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<AudioManager>();
            if (_instance == null)
                _instance = new GameObject("Audio Manager").AddComponent<AudioManager>();

            return _instance;
        }
    }

    [SerializeField] private AudioController _audioController;

    public AudioController AudioController
    {
        get
        {
            if (_audioController == null)
                _audioController = Resources.Load<AudioController>("Audio Controller");
            return _audioController;
        }
    }
    public static AudioMixer AudioMixer => Instance.AudioController.AudioMixer;

    public static SFXList SFXList => Instance.AudioController.SFXList;

    public static MusicList MusicList => Instance.AudioController.MusicList;

    public static VoiceList VoiceList => Instance.AudioController.VoiceList;

    private static AudioSource _musicAudioSource;
    public static AudioSource MusicAudioSource
    {
        get
        {
            if (_musicAudioSource == null)
            {
                _musicAudioSource = new GameObject("Music Audio").AddComponent<AudioSource>();
                _musicAudioSource.spatialBlend = 0;
                _musicAudioSource.playOnAwake = false;
                _musicAudioSource.loop = true;
                _musicAudioSource.transform.SetParent(Instance.transform);
                _musicAudioSource.outputAudioMixerGroup = Instance.AudioController.AudioMixer.FindMatchingGroups("Music")[0];
            }

            return _musicAudioSource;
        }
    }

    private static AudioSource _voiceAudioSource;
    public static AudioSource VoiceAudioSource
    {
        get
        {
            if (_voiceAudioSource == null)
            {
                _voiceAudioSource = new GameObject("Voice Audio").AddComponent<AudioSource>();
                _voiceAudioSource.spatialBlend = 0;
                _voiceAudioSource.playOnAwake = false;
                _voiceAudioSource.loop = false;
                _voiceAudioSource.transform.SetParent(Instance.transform);
                _voiceAudioSource.outputAudioMixerGroup = Instance.AudioController.AudioMixer.FindMatchingGroups("Voice")[0];
            }

            return _voiceAudioSource;
        }
    }
    public static AudioClip CurrentMusic
    {
        get
        {
            if (!MusicAudioSource.isPlaying)
                return null;

            return MusicAudioSource.clip;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public static void PlaySFX(AudioClip audio, System.Action callback = null)
    {
        SFX sfx = Instance._sfxs.Find((s) => s.Clip == audio && !s.AudioSource.isPlaying);
        if (sfx == null)
        {
            sfx = new SFX(audio);
            Instance._sfxs.Add(sfx);
        }
        sfx.AudioSource.Stop();
        sfx.AudioSource.Play();
    }

    public static void PlayMusic(AudioClip music)
    {
        PlayMusic(music, false);
    }

    public static void PlayMusic(AudioClip music, bool reset)
    {
        if (MusicAudioSource.clip == music && !reset)
            return;

        MusicAudioSource.Stop();
        MusicAudioSource.clip = music;
        MusicAudioSource.Play();
    }

    public static void StopMusic()
    {
        MusicAudioSource.Stop();
    }

    public static void PlayVoice(AudioClip voice, System.Action callback = null)
    {
        VoiceAudioSource.Stop();
        VoiceAudioSource.clip = voice;
        VoiceAudioSource.Play();
        if (callback != null)
            CoroutineManager.WaitForSeconds(VoiceAudioSource.clip.length, callback);
    }

    public static void PlayVoice(AudioClip voice, float delay, System.Action callback = null)
    {
        CoroutineManager.WaitForSeconds(delay, () =>
        {
            PlayVoice(voice, callback);
        });
    }

    public static void PlayVoice(string name, System.Action callback = null)
    {
        AudioClip voice = Instance.AudioController.VoiceList.GetVoiceClip(name);
        PlayVoice(voice, callback);
    }
}

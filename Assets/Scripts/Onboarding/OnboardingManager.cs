using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnboardingManager : GameManager
{
    [Header("Onboarding")]
    [SerializeField] private GameObject _loadingContainer;
    [SerializeField] private GameObject _authenticationContainer;
    [SerializeField] private GameObject _welcomeContainer;
    [SerializeField] private QRReader _qrReader;

    [Header("Welcome")]
    [SerializeField] private GameObject _welcomeStepContainer;
    [SerializeField] private Text _welcomeText;
    [TextArea] [SerializeField] private List<string> _welcomeTexts;

    [Header("Name")]
    [SerializeField] private GameObject _nameStepContainer;
    [SerializeField] private InputField _nameInputField;
    [SerializeField] private Button _confirmNameButton;

    [Header("Avatar")]
    [SerializeField] private GameObject _avatarStepContainer;
    [SerializeField] private CharactersData _charactersData;
    [SerializeField] private Image _avatarImage;
    [SerializeField] private Text _avatarName;
    [SerializeField] private Text _avatarInfoText;
    [SerializeField] private Button _confirmAvatarButton;

    [Header("Footer")]
    [SerializeField] private GameObject _footerContainer;
    [SerializeField] private Text _pageText;
    [SerializeField] private Button _prevPageButton;
    [SerializeField] private Button _nextPageButton;

    private int _currentStep;
    private int _currentPage;
    private int _currentPageCount;

    private void Awake()
    {
        _prevPageButton.onClick.AddListener(OnPrevPageButtonClicked);
        _nextPageButton.onClick.AddListener(OnNextPageButtonClicked);
        _confirmNameButton.onClick.AddListener(OnConfirmNameButtonClicked);
        _confirmAvatarButton.onClick.AddListener(OnConfirmAvatarButtonClicked);

        _loadingContainer.SetActive(true);
    }

    public void OnAuthentication()
    {
        _authenticationContainer.SetActive(false);

        if (!string.IsNullOrEmpty(PlayerData.Name))
        {
            OnConfirmAvatarButtonClicked();
            return;
        }

        _loadingContainer.SetActive(false);
        _welcomeContainer.SetActive(true);
        SetupCurrentStep();
    }

    private void NextStep()
    {
        _currentStep++;
        SetupCurrentStep();
    }

    private void SetupCurrentStep()
    {
        _welcomeStepContainer.SetActive(_currentStep == 0);
        _nameStepContainer.SetActive(_currentStep == 1);
        _avatarStepContainer.SetActive(_currentStep == 2);

        _currentPage = 0;

        switch (_currentStep)
        {
            default:
                break;
            case 0:
                _footerContainer.SetActive(true);
                _currentPageCount = _welcomeTexts.Count;
                break;
            case 1:
                _footerContainer.SetActive(false);
                break;
            case 2:
                _footerContainer.SetActive(true);
                _currentPageCount = _charactersData.Characters.Count;
                break;
        }

        UpdatePage();
    }

    private void UpdateWelcomeText()
    {
        _welcomeText.text = _welcomeTexts[_currentPage];
    }

    private void UpdateAvatarData()
    {
        var charData = _charactersData.GetCharacterData(_currentPage);

        _avatarImage.sprite = charData.Sprite;
        _avatarName.text = charData.Name;
        _avatarInfoText.text = charData.Description;
    }

    public void OnNextPageButtonClicked()
    {
        _currentPage++;

        if (_currentStep == 0 && _currentPage >= _currentPageCount)
        {
            NextStep();
            return;
        }

        _currentPage %= _currentPageCount;

        UpdatePage();
    }

    public void OnPrevPageButtonClicked()
    {
        _currentPage--;

        if (_currentStep == 0 && _currentPage < 0 )
            _currentPage = 0;
        else
            _currentPage = _currentPage < 0 ? _currentPageCount - 1 : _currentPage;

        UpdatePage();
    }

    public void OnConfirmNameButtonClicked()
    {
        if (_nameInputField.text.Length <= 0)
            return;

        PlayerData.Name = _nameInputField.text;

        NextStep();
    }

    public void OnConfirmAvatarButtonClicked()
    {
        PlayerData.AvatarIndex = _currentPage;

        BackToMainMenu();
    }

    private void UpdatePage()
    {
        _pageText.text = $"{_currentPage + 1} / {_currentPageCount}";

        switch (_currentStep)
        {
            default:
                break;
            case 0:
                UpdateWelcomeText();
                break;
            case 2:
                UpdateAvatarData();
                break;
        }
    }
}

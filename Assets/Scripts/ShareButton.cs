using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ShareButton : MonoBehaviour
{
    private string _lastScreenShotPath;
    public UnityAction OnShareCompleted;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(OnShareButtonClicked);
    }

    public void TakeScreenshot()
    {
        StartCoroutine(CaptureScreenShot());
    }

    public void SetScreenshotPath(string path)
    {
        _lastScreenShotPath = path;
    }

    public void OnShareButtonClicked()
    {
        if (string.IsNullOrEmpty(_lastScreenShotPath))
            StartCoroutine(TakeScreenshotAndShare());
        else
            Share();
    }

    private void Share()
    {
        new NativeShare()
                .AddFile(_lastScreenShotPath)
                .SetCallback((result, target) => 
                    { 
                        OnShareCompleted?.Invoke(); 
                        Debug.LogWarning($"Share result: {result}, selected target: {target}"); 
                    })
                .Share();

        _lastScreenShotPath = string.Empty;
    }

    IEnumerator TakeScreenshotAndShare()
    {
        yield return CaptureScreenShot();

        Share();
    }

    IEnumerator CaptureScreenShot()
    {
        Button.interactable = false;

        yield return new WaitForEndOfFrame();
        System.DateTime time = System.DateTime.Now;
        string path = $"{Application.persistentDataPath}/{Application.productName}-{time.Day}-{time.Month}-{time.Year}_{time.Hour}-{time.Minute}.png";

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        byte[] imageBytes = screenImage.EncodeToPNG();

        System.IO.File.WriteAllBytes(path, imageBytes);

        _lastScreenShotPath = path;
        Button.interactable = true;
    }
}

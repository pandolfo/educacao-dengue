using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CertificateManager : GameManager
{
    [Header("Certificate")]
    [SerializeField] private CharactersData _charactersData;
    [SerializeField] private ChallengesData _challengesData;
    [SerializeField] private Image _avatarImage;
    [SerializeField] private Text _avatarName;
    [SerializeField] private Text _abilitiesText;

    private void Start()
    {
        var charData = _charactersData.GetCharacterData(PlayerData.AvatarIndex);
        _avatarImage.sprite = charData.Sprite;
        _avatarName.text = PlayerData.Name;

        int stars = Mathf.FloorToInt(PlayerData.GetTotalStars() / (_challengesData.ChallengesCount));
        for (int i = 0; i < _stars.Count; i++)
        {
            _stars[i].SetActive(i < stars);
        }

        AudioManager.PlayMusic(AudioManager.MusicList.MainMenu);
    }
}

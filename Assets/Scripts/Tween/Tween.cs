using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

namespace PotaTween
{
    #region Classes
    [Serializable]
    public class PTTVector3
    {
        public Vector3 From, To;
        public bool IsLocal, IsRelative;
        public AxisMask IgnoreAxis;

        public PTTVector3(Vector3 value)
        {
            this.From = value;
            this.To = value;
        }

        public PTTVector3(Vector3 from, Vector3 to)
        {
            this.From = from;
            this.To = to;
        }

        public PTTVector3(Vector3 from, Vector3 to, bool isLocal)
        {
            this.From = from;
            this.To = to;
            this.IsLocal = isLocal;
        }

        public PTTVector3(Vector3 from, Vector3 to, bool isLocal, bool isRelative)
        {
            this.From = from;
            this.To = to;
            this.IsLocal = isLocal;
            this.IsRelative = isRelative;
        }

        public PTTVector3(Vector3 from, Vector3 to, bool isLocal, bool isRelative, AxisMask ignoreAxis)
        {
            this.From = from;
            this.To = to;
            this.IsLocal = isLocal;
            this.IsRelative = isRelative;
            this.IgnoreAxis = ignoreAxis;
        }
    }

    [Serializable]
    public class PTTFloat
    {
        public float From, To;
        [HideInInspector]
        public float Value;

        public PTTFloat(float value)
        {
            this.From = value;
            this.To = value;
        }

        public PTTFloat(float from, float to)
        {
            this.From = from;
            this.To = to;
        }
    }

    [Serializable]
    public class PTTColor
    {
        public Color From, To;

        public PTTColor(Color value)
        {
            this.From = value;
            this.To = value;
        }

        public PTTColor(Color from, Color to)
        {
            this.From = from;
            this.To = to;
        }
    }

    public class PTTTimedAction
    {
        public Action Action;
        public float Time;

        public PTTTimedAction(Action action, float time)
        {
            this.Action = action;
            this.Time = time;
        }
    }

    [Serializable]
    public struct AxisMask
    {
        public bool X, Y, Z;

        public bool All
        {
            get { return (X && Y && Z); }
            set { X = value; Y = value; Z = value; }
        }

        public bool None
        {
            get { return (!X && !Y && !Z); }
        }

        public AxisMask(bool x, bool y, bool z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
    }

    #endregion

    #region Enums
    public enum LoopType
    {
        None,
        Loop,
        PingPong
    }

    public enum TweenAxis
    {
        X, Y, Z
    }

    public enum TweenType
    {
        From, To
    }

    public enum EasingReference
    {
        Equation,
        Curve
    }
    #endregion

    public class Tween : MonoBehaviour
    {

#if UNITY_EDITOR
        [HideInInspector]
        public bool foldout = true;
#endif

        [Serializable]
        public class PotaTweenEvent : UnityEvent<Tween> { }

        public string Tag;
        public int Id;

        public bool PlayOnStart;
        public bool PlayOnEnable;

        [Header("Time")]
        public float Duration = 1;
        [Tooltip("Speed overwrite Duration")]
        public float Speed;
        public float Delay;

        [Header("Loop")]
        public LoopType Loop;
        public int LoopsNumber;

        [Header("Easing")]
        public EasingReference EasingReference;
        public Ease.Equation EaseEquation;
        public AnimationCurve Curve;

        [Tooltip("Reverses the curve when the loop is set to PingPong")]
        public bool FlipCurveOnReverse = true;

        [Header("Properties")]
        [SerializeField]
        protected PTTVector3 _position;
        [SerializeField]
        protected PTTVector3 _rotation;
        [SerializeField]
        protected PTTVector3 _scale;
        [SerializeField]
        protected PTTColor _color;
        [SerializeField]
        protected PTTFloat _alpha;
        [SerializeField]
        protected PTTFloat _float;

        private bool _hasReverted = false;

        private Rigidbody _rigidbody;
        private Rigidbody2D _rigidbody2D;

        private Vector3 _startPosition, _startRotation, _startScale;

        #region Properties
        public float ElapsedDelay { get; set; }
        public float ElapsedTime { get; set; }

        [Header("Debug")]
        [SerializeField]
        protected bool _isPlaying;
        public bool IsPlaying
        {
            get { return _isPlaying; }
        }

        [SerializeField]
        protected bool _isReversing;
        public bool IsReversing
        {
            get { return _isReversing; }
        }

        [SerializeField]
        protected bool _isPaused;
        public bool IsPaused
        {
            get { return _isPaused; }
        }

        protected bool _hasCompleted;
        public bool HasCompleted
        {
            get { return _hasCompleted; }
        }

        private int _loopcount;
        public int LoopCount
        {
            get { return _loopcount; }
        }

        public GameObject Target
        {
            get { return gameObject; }
        }

        private Transform _ownTransform;
        public Transform OwnTransform
        {
            get
            {
                if (_ownTransform == null)
                    _ownTransform = transform;
                return _ownTransform;
            }
        }

        public float FloatValue
        {
            get
            {
                return _float.Value;
            }
        }
        #endregion

        [Header("Events")]
        [SerializeField]
        protected PotaTweenEvent _onStart = new PotaTweenEvent();
        public PotaTweenEvent onStart
        {
            get { return _onStart; }
            set { _onStart = value; }
        }

        [SerializeField]
        protected PotaTweenEvent _onComplete = new PotaTweenEvent();
        public PotaTweenEvent onComplete
        {
            get { return _onComplete; }
            set { _onComplete = value; }
        }

        protected void Awake()
        {
            if ((Curve == null || Curve.keys.Length <= 1) && EasingReference != EasingReference.Equation)
            {
                EasingReference = EasingReference.Equation;
                EaseEquation = Ease.Equation.Linear;
            }

            easeMethod = System.Type.GetType("PotaTween.Ease").GetMethod(EaseEquation.ToString());

            _colorProperty = "_Color";
            GetRenderers();

            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody2D = GetComponent<Rigidbody2D>();

            _startPosition = OwnTransform.position;
            _startRotation = OwnTransform.eulerAngles;
            _startScale = OwnTransform.localScale;
        }

        protected void Start()
        {
            if (PlayOnStart)
                Play();
        }

        protected void OnEnable()
        {
            if (PlayOnEnable)
            {
                if (!_hasReverted)
                    Play();
                else
                    Reverse();
            }
        }

        protected void OnDisable()
        {
            Stop();
        }

        protected void Update()
        {
            if (!IsPlaying || IsPaused)
                return;

            if (DelayLoop())
                return;

            if (TweenLoop())
                return;

            TweenCompleted();
        }

        #region Create/Get
        /// <summary>
        /// Creates a new PotaTween, or returns an existing with the same target and id.
        /// </summary>
        /// <param name="target">GameObject that the tween will be attached to.</param>
        /// <param name="id">Id of the tween for reference.</param>
        public static Tween Create(GameObject target, int id = 0)
        {
            Tween tween;

            tween = Get(target, id);

            if (tween == null)
            {
                tween = target.AddComponent<Tween>();

                tween.Id = id;
                tween.Initialize();
            }

            return tween;
        }

        public static Tween Create(GameObject target, string tag)
        {
            Tween tween;

            tween = Get(target, tag);

            if (tween == null)
            {
                tween = target.AddComponent<Tween>();

                tween.Tag = tag;
                tween.Initialize();
            }

            return tween;
        }

        public static Tween Get(GameObject target, int id = 0)
        {
            Tween[] tweens = target.GetComponents<Tween>();

            if (tweens != null)
            {
                for (int i = 0; i < tweens.Length; i++)
                {
                    if (tweens[i].Id == id)
                    {
                        return tweens[i];
                    }
                }
            }

            return null;
        }

        public static Tween Get(GameObject target, string tag)
        {
            Tween[] tweens = target.GetComponents<Tween>();

            if (tweens != null)
            {
                for (int i = 0; i < tweens.Length; i++)
                {
                    if (tweens[i].Tag == tag)
                    {
                        return tweens[i];
                    }
                }
            }

            return null;
        }
        #endregion

        #region Setters
        #region Position
        public Tween SetPosition(Vector3 from, Vector3 to, bool isLocal = false, bool isRelative = false)
        {
            _position = new PTTVector3(from, to, isLocal, isRelative);
            _position.IgnoreAxis = new AxisMask(false, false, false);

            return this;
        }

        public Tween SetPosition(TweenAxis axis, float from, float to, bool isLocal = false, bool isRelative = false)
        {
            if (_position.IgnoreAxis.None)
                _position.IgnoreAxis = new AxisMask(true, true, true);

            switch (axis)
            {
                case TweenAxis.X:
                    _position.IgnoreAxis.X = false;
                    _position.From.x = from;
                    _position.To.x = to;
                    break;

                case TweenAxis.Y:
                    _position.IgnoreAxis.Y = false;
                    _position.From.y = from;
                    _position.To.y = to;
                    break;

                case TweenAxis.Z:
                    _position.IgnoreAxis.Z = false;
                    _position.From.z = from;
                    _position.To.z = to;
                    break;
            }

            _position.IsLocal = isLocal;
            _position.IsRelative = isRelative;

            return this;
        }
        #endregion

        #region Rotation
        public Tween SetRotation(Vector3 from, Vector3 to, bool isLocal = false, bool isRelative = false)
        {
            _rotation = new PTTVector3(from, to, isLocal, isRelative);
            _rotation.IgnoreAxis = new AxisMask(false, false, false);

            return this;
        }

        public Tween SetRotation(TweenAxis axis, float from, float to, bool isLocal = false, bool isRelative = false)
        {
            if (_rotation.IgnoreAxis.None)
                _rotation.IgnoreAxis = new AxisMask(true, true, true);

            switch (axis)
            {
                case TweenAxis.X:
                    _rotation.IgnoreAxis.X = false;
                    _rotation.From.x = from;
                    _rotation.To.x = to;
                    break;

                case TweenAxis.Y:
                    _rotation.IgnoreAxis.Y = false;
                    _rotation.From.y = from;
                    _rotation.To.y = to;
                    break;

                case TweenAxis.Z:
                    _rotation.IgnoreAxis.Z = false;
                    _rotation.From.z = from;
                    _rotation.To.z = to;
                    break;
            }

            _rotation.IsLocal = isLocal;
            _rotation.IsRelative = isRelative;

            return this;
        }
        #endregion

        #region Scale
        public Tween SetScale(Vector3 from, Vector3 to, bool isRelative = false)
        {
            _scale = new PTTVector3(from, to, false, isRelative);
            _scale.IgnoreAxis = new AxisMask(false, false, false);

            return this;
        }

        public Tween SetScale(TweenAxis axis, float from, float to, bool isRelative = false)
        {
            if (_scale.IgnoreAxis.None)
                _scale.IgnoreAxis = new AxisMask(true, true, true);

            switch (axis)
            {
                case TweenAxis.X:
                    _scale.IgnoreAxis.X = false;
                    _scale.From.x = from;
                    _scale.To.x = to;
                    break;

                case TweenAxis.Y:
                    _scale.IgnoreAxis.Y = false;
                    _scale.From.y = from;
                    _scale.To.y = to;
                    break;

                case TweenAxis.Z:
                    _scale.IgnoreAxis.Z = false;
                    _scale.From.z = from;
                    _scale.To.z = to;
                    break;
            }

            _scale.IsRelative = isRelative;

            return this;
        }
        #endregion

        #region Color
        private Renderer[] _renderers;
        private UnityEngine.UI.MaskableGraphic[] _images;
        private Material[] _bkpMaterials;
        private string _colorProperty;
        private bool _isVertexColor;

        #region Color
        public Tween SetColor(Color from, Color to, string colorProperty = "_Color")
        {
            _color = new PTTColor(from, to);
            this._colorProperty = colorProperty;
            GetRenderers();

            return this;
        }
        #endregion

        #region Alpha
        public Tween SetAlpha(float from, float to, bool vertexColor = false, string colorProperty = "_Color")
        {
            _alpha = new PTTFloat(from, to);
            this._colorProperty = colorProperty;
            this._isVertexColor = vertexColor;
            GetRenderers();

            return this;
        }

        public Tween SetAlpha(TweenType type, float value, string colorProperty = "_Color")
        {
            this._colorProperty = colorProperty;

            Renderer tempRender = GetComponent<Renderer>() ? GetComponent<Renderer>() : GetComponentInChildren<Renderer>();
            Image tempImageRender = GetComponentInChildren<Image>();

            float targetAlpha = tempImageRender != null ? tempImageRender.color.a : tempRender.sharedMaterial.GetColor(colorProperty).a;
            if (tempRender is SpriteRenderer)
            {
                targetAlpha = ((SpriteRenderer)tempRender).color.a;
            }

            switch (type)
            {
                case TweenType.From:
                    _alpha = new PTTFloat(value, targetAlpha);
                    break;

                case TweenType.To:
                    _alpha = new PTTFloat(targetAlpha, value);
                    break;
            }

            GetRenderers();
            return this;
        }
        #endregion

        private void GetRenderers()
        {
            _renderers = GetComponentsInChildren<Renderer>(true);

            _images = GetComponentsInChildren<UnityEngine.UI.MaskableGraphic>(true);

            _bkpMaterials = new Material[_renderers.Length];

            for (int i = 0; i < _bkpMaterials.Length; i++)
            {
                _bkpMaterials[i] = _renderers[i].sharedMaterial;
            }
        }
        #endregion

        #region Float
        public Tween SetFloat(float from, float to)
        {
            _float = new PTTFloat(from, to);

            return this;
        }

        public Tween SetFloat(float from, float to, Action updateCallback)
        {
            _float = new PTTFloat(from, to);
            this._updateCallback = updateCallback;

            return this;
        }
        #endregion

        #region Duration
        public Tween SetDuration(float time)
        {
            Duration = time;
            return this;
        }
        public void SetDurationEvent(float time)
        {
            Duration = time;
        }
        #endregion

        #region Speed
        public Tween SetSpeed(float speed)
        {
            this.Speed = speed;

            CalculateDuration();

            return this;
        }
        public void SetSpeedEvent(float speed)
        {
            this.Speed = speed;

            CalculateDuration();
        }
        #endregion

        #region Delay
        public Tween SetDelay(float time)
        {
            Delay = time;
            return this;
        }

        public void SetDelayEvent(float time)
        {
            Delay = time;
        }
        #endregion

        #region Loop
        public Tween SetLoop(LoopType loop)
        {
            Loop = loop;
            return this;
        }

        public Tween SetLoop(LoopType loop, int loopsNumber)
        {
            Loop = loop;
            LoopsNumber = loopsNumber;
            return this;
        }
        #endregion

        #region Ease
        float _easeValue;
        System.Reflection.MethodInfo easeMethod = null;
        public Tween SetEaseEquation(Ease.Equation easeEquation)
        {
            EaseEquation = easeEquation;
            EasingReference = EasingReference.Equation;
            easeMethod = System.Type.GetType("PotaTween.Ease").GetMethod(EaseEquation.ToString());
            return this;
        }
        #endregion

        #region Curve
        public Tween SetCurve(AnimationCurve curve)
        {
            Curve = curve;
            EasingReference = EasingReference.Curve;
            return this;
        }
        #endregion

        #region Callbacks
        Action _startCallback, _updateCallback;
        private Action<GameObject> finishCallback;
        public Tween StartCallback(Action callback)
        {
            _startCallback = callback;
            return this;
        }

        public Tween UpdateCallback(Action callback)
        {
            _updateCallback = callback;
            return this;
        }

        public Tween FinishCallback(Action<GameObject> callback)
        {
            finishCallback = callback;
            return this;
        }

        List<PTTTimedAction> timedCallbacks;
        public Tween TimedCallback(Action callback, float approxTime)
        {
            if (timedCallbacks == null)
                timedCallbacks = new List<PTTTimedAction>();

            timedCallbacks.Add(new PTTTimedAction(callback, approxTime));
            return this;
        }

        public Tween RemoveAllTimedCallbacks()
        {
            timedCallbacks = new List<PTTTimedAction>();
            return this;
        }
        #endregion

        #endregion

        #region Functions
        Action callback;
        /// <summary>
        /// Plays the animation.
        /// </summary>
        public void Play()
        {
            Play(null);
        }
        /// <summary>
        /// Plays the animation
        /// </summary>
        /// <param name="callback">The callback function to be called at the end of the animation</param>
        public void Play(Action callback)
        {
            if (!Target.activeSelf) return;

            this.callback = callback;

            _isPaused = false;

            if (!IsPlaying)
            {
                _isPlaying = true;
                _isReversing = false;
                onStart.Invoke(this);

                if (_hasReverted)
                {
                    ReverseDirection();
                }
                _loopcount = 0;

                StartTween();
            }
        }

        /// <summary>
        /// Plays the animation with the specified tag.
        /// </summary>
        /// <param name="tag">Tag of the animation.</param>
        public void PlayWithTag(string tag)
        {
            if (Tag == tag)
                Play();
            else
                GetTweenWithTag(tag).Play();
        }

        /// <summary>
        /// Plays the animation in reverse
        /// </summary>
        public void Reverse()
        {
            Reverse(null);
        }
        /// <summary>
        /// Plays the animation in reverse
        /// </summary>
        /// <param name="callback">The callback function to be called at the end of the animation</param>
        public void Reverse(Action callback)
        {
            if (!Target.activeSelf) return;

            this.callback = callback;

            _isPaused = false;

            if (!IsPlaying)
            {
                _isPlaying = _isReversing = true;
                onStart.Invoke(this);

                if (!_hasReverted)
                {
                    ReverseDirection();
                }

                StartTween();
            }
        }

        /// <summary>
        /// Plays the animation in reverse with the specified tag.
        /// </summary>
        /// <param name="tag">Tag of the animation.</param>
        public void ReverseWithTag(string tag)
        {
            if (Tag == tag)
                Reverse();
            else
                GetTweenWithTag(tag).Reverse();
        }

        public void Pause()
        {
            _isPaused = true;
        }

        public void PauseWithTag(string tag)
        {
            if (Tag == tag)
                Pause();
            else
                GetTweenWithTag(tag).Pause();
        }

        public void Stop()
        {
            _isPlaying = false;
            _isReversing = false;

            ElapsedDelay = 0;
            ElapsedTime = 0;
        }

        public void StopWithTag(string tag)
        {
            if (Tag == tag)
                Stop();
            else
                GetTweenWithTag(tag).Stop();
        }

        public void Reset()
        {
            Stop();
            UpdateAll(0);
        }

        public void Clear()
        {
            Initialize();
        }

        public void InitialState()
        {
            UpdateAll(0);
        }

        public void FinalState()
        {
            UpdateAll(1);
        }

        private Tween GetTweenWithTag(string tag)
        {
            foreach (Tween tween in GetComponents<Tween>())
            {
                if (tween.Tag == tag)
                {
                    return tween;
                }
            }

            Debug.LogError("Tween with tag \"" + tag + "\" not found.");
            return null;
        }

        #endregion

        #region Initialize
        void Initialize()
        {
            _position = new PTTVector3(OwnTransform.position);
            _rotation = new PTTVector3(OwnTransform.eulerAngles);
            _scale = new PTTVector3(OwnTransform.localScale);
            if (GetComponent<Renderer>() != null && GetComponent<Renderer>().sharedMaterial != null)
            {
                _color =
                    new PTTColor((GetComponent<Renderer>().sharedMaterial.HasProperty(_colorProperty)
                                  ? GetComponent<Renderer>().sharedMaterial.GetColor(_colorProperty)
                                  : UnityEngine.Color.white));
                _alpha =
                    new PTTFloat((GetComponent<Renderer>().sharedMaterial.HasProperty(_colorProperty)
                                  ? GetComponent<Renderer>().sharedMaterial.GetColor(_colorProperty).a
                                  : 1));
            }
            else
            {
                _color = new PTTColor(UnityEngine.Color.white);
                _alpha = new PTTFloat(1);
            }
            _float = new PTTFloat(0);

            Duration = 1;
            Speed = 0;
            Delay = 0;
            SetEaseEquation(Ease.Equation.Linear);
            _startCallback = null;
            _updateCallback = null;
            finishCallback = null;
            _hasReverted = false;

            _loopcount = 0;

            Curve = AnimationCurve.Linear(0, 0, 1, 1);
            Loop = LoopType.None;
        }
        #endregion

        #region Tween
        void StartTween()
        {
            StartFrom(0);
        }

        void StartFrom(float time)
        {
            ElapsedTime = time;
            ElapsedDelay = time;

            CalculateDuration();

            GetRenderers();
            InstantiateMaterials();

            UpdateAll(CalculateEase());

            _isPlaying = true;
            _hasCompleted = false;

            if (_startCallback != null)
                _startCallback();
        }

        private bool DelayLoop()
        {
            if (ElapsedDelay < Delay)
            {
                ElapsedDelay += Time.deltaTime;
                return true;
            }

            return false;
        }

        private bool TweenLoop()
        {
            if (ElapsedTime < Duration)
            {
                ElapsedTime += Time.deltaTime;
                UpdateAll(CalculateEase());
                if (_updateCallback != null)
                    _updateCallback();
                CheckTimedCallback();

                return true;
            }

            return false;
        }

        private void TweenCompleted()
        {
            if (!HasCompleted && IsPlaying)
            {
                _hasCompleted = true;

                ElapsedTime = Duration;
                UpdateAll(1);

                _isPlaying = false;
                onComplete.Invoke(this);

                if (callback != null)
                {
                    callback();
                    //callback = null;
                }

                if (finishCallback != null)
                    finishCallback(gameObject);

                ReturnMaterials();

                if (Loop != LoopType.None)
                {
                    _loopcount++;

                    if (LoopsNumber == 0 || _loopcount < LoopsNumber)
                    {
                        if (Loop == LoopType.PingPong)
                            ReverseDirection();

                        StartTween();
                    }
                }
            }
        }
        #endregion

        #region CalculateDuration
        float positionDuration, rotationDuration, scaleDuration, colorDuration, alphaDuration;
        List<float> durationsList = new List<float>();
        void CalculateDuration()
        {
            if (Speed <= 0) return;

            durationsList = new List<float>();

            if (_position.From != _position.To)
            {
                positionDuration = Mathf.Abs(Vector3.Distance(_position.From, _position.To) / Speed);
                durationsList.Add(positionDuration);
            }
            if (_scale.From != _scale.To)
            {
                scaleDuration = Mathf.Abs(Vector3.Distance(_scale.From, _scale.To) / Speed);
                durationsList.Add(scaleDuration);
            }
            if (_rotation.From != _rotation.To)
            {
                rotationDuration = Mathf.Abs((Vector3.Angle(_rotation.From, _rotation.To)) / Speed);
                durationsList.Add(rotationDuration);
            }
            if (_color.From != _color.To)
            {
                colorDuration = Mathf.Abs(Vector4.Distance((Vector4)_color.From, (Vector4)_color.To) / Speed);
                durationsList.Add(colorDuration);
            }
            if (_alpha.From != _alpha.To)
            {
                alphaDuration = Mathf.Abs((_alpha.From - _alpha.To) / Speed);
                durationsList.Add(alphaDuration);
            }

            if (durationsList.Count > 0)
                Duration = durationsList.Max();
        }
        #endregion

        #region Updates
        void UpdateAll(float value)
        {
            _easeValue = value;

            if (Target == null) return;

            UpdatePosition();
            UpdateRotation();
            UpdateScale();
            UpdateColor();
            UpdateAlpha();
            UpdateFloat();
        }

        float CalculateEase()
        {
            if (ElapsedTime > Duration)
                ElapsedTime = Duration;

            if (easeMethod == null)
                easeMethod = System.Type.GetType("Ease").GetMethod(EaseEquation.ToString());

            return (EasingReference == EasingReference.Equation) ? (float)easeMethod.Invoke(null, new object[] { ElapsedTime, 0, 1, Duration }) :
                Curve.Evaluate(ElapsedTime / Duration);
        }

        float CalculateLerp(float from, float to)
        {
            return from + _easeValue * (to - from);
        }

        #region Position
        void UpdatePosition()
        {
            if (_position == null || _position.From == _position.To || ((OwnTransform.position == _position.To || OwnTransform.localPosition == _position.To) && ElapsedTime >= Duration)) return;

            RectTransform rectT = GetComponent<RectTransform>();
            if (rectT != null)
            {
                Vector3 rp = rectT.anchoredPosition;

                rp.y = _position.IgnoreAxis.Y ? rp.y : CalculateLerp(_position.From.y, _position.To.y);
                rp.x = _position.IgnoreAxis.X ? rp.x : CalculateLerp(_position.From.x, _position.To.x);
                rp.z = _position.IgnoreAxis.Z ? rp.z : CalculateLerp(_position.From.z, _position.To.z);

                rectT.anchoredPosition = rp;

                return;
            }

            Vector3 p = _position.IsLocal ? OwnTransform.localPosition : OwnTransform.position;
            Vector3 add = _position.IsRelative ? _startPosition : Vector3.zero;

            p.x = _position.IgnoreAxis.X ? p.x : CalculateLerp(_position.From.x, _position.To.x) + add.x;
            p.y = _position.IgnoreAxis.Y ? p.y : CalculateLerp(_position.From.y, _position.To.y) + add.y;
            p.z = _position.IgnoreAxis.Z ? p.z : CalculateLerp(_position.From.z, _position.To.z) + add.z;

            if (_position.IsLocal)
            {
                OwnTransform.localPosition = p;
            }
            else
            {
                if (_rigidbody2D != null)
                    _rigidbody2D.MovePosition(p);
                else if (_rigidbody != null)
                    _rigidbody.MovePosition(p);
                else
                    OwnTransform.position = p;
            }
        }
        #endregion

        #region Rotation
        void UpdateRotation()
        {
            if (_rotation == null || _rotation.From == _rotation.To || (OwnTransform.eulerAngles == _rotation.To && ElapsedTime >= Duration)) return;

            Vector3 r = _rotation.IsLocal ? OwnTransform.localEulerAngles : OwnTransform.eulerAngles;
            Vector3 add = _rotation.IsRelative ? _startRotation : Vector3.zero;

            r.x = _rotation.IgnoreAxis.X ? r.x : CalculateLerp(_rotation.From.x, _rotation.To.x) + add.x;
            r.y = _rotation.IgnoreAxis.Y ? r.y : CalculateLerp(_rotation.From.y, _rotation.To.y) + add.y;
            r.z = _rotation.IgnoreAxis.Z ? r.z : CalculateLerp(_rotation.From.z, _rotation.To.z) + add.z;

            if (_rotation.IsLocal)
                OwnTransform.localRotation = Quaternion.Euler(r);
            else
                OwnTransform.rotation = Quaternion.Euler(r);
            //print(OwnTransform.localRotation.eulerAngles);
        }
        #endregion

        #region Scale
        void UpdateScale()
        {
            if (_scale == null || _scale.From == _scale.To || (OwnTransform.localScale == _scale.To && ElapsedTime >= Duration)) return;

            Vector3 s = OwnTransform.localScale;
            Vector3 add = _scale.IsRelative ? _startScale : Vector3.zero;

            s.x = _scale.IgnoreAxis.X ? s.x : CalculateLerp(_scale.From.x, _scale.To.x) + add.x;
            s.y = _scale.IgnoreAxis.Y ? s.y : CalculateLerp(_scale.From.y, _scale.To.y) + add.y;
            s.z = _scale.IgnoreAxis.Z ? s.z : CalculateLerp(_scale.From.z, _scale.To.z) + add.z;

            OwnTransform.localScale = s;
        }
        #endregion

        #region Color
        void UpdateColor()
        {
            if (_color == null || _color.From == _color.To) return;

            Color newColor = _color.From + _easeValue * (_color.To - _color.From);

            for (int i = 0; i < _renderers.Length; i++)
            {
                if (_renderers[i] == null) continue;

                TextMesh tm = _renderers[i].GetComponent<TextMesh>();

                if (tm)
                {
                    tm.color = newColor;
                }
                else if (_renderers[i] is SpriteRenderer)
                {
                    SpriteRenderer render = (SpriteRenderer)_renderers[i];
                    render.color = newColor;
                }
                else if (_renderers[i].sharedMaterial.HasProperty(_colorProperty))
                {
                    _renderers[i].sharedMaterial.SetColor(_colorProperty, newColor);
                }
            }

            if (_images == null)
                return;

            for (int j = 0; j < _images.Length; j++)
            {
                _images[j].color = newColor;
            }
        }

        void InstantiateMaterials()
        {
            if (_color.From == _color.To && _alpha.From == _alpha.To) return;

            List<Material> refMaterials = new List<Material>();
            for (int i = 0; i < _renderers.Length; i++)
            {
                if (!refMaterials.Contains(_renderers[i].sharedMaterial))
                {
                    refMaterials.Add(_renderers[i].sharedMaterial);
                }
            }

            List<Material> cloneMaterials = new List<Material>();
            for (int i = 0; i < refMaterials.Count; i++)
            {
                cloneMaterials.Add(refMaterials[i] == null ? null : new Material(refMaterials[i]));
            }

            for (int i = 0; i < _renderers.Length; i++)
            {
                for (int j = 0; j < refMaterials.Count; j++)
                {
                    if (_renderers[i].sharedMaterial != null && _renderers[i].sharedMaterial == refMaterials[j])
                    {
                        _renderers[i].sharedMaterial = cloneMaterials[j];
                    }
                }
            }
        }

        void ReturnMaterials()
        {
            for (int i = 0; i < _renderers.Length; i++)
            {
                if (_renderers[i] != null && _bkpMaterials[i] != null && _renderers[i].sharedMaterial != null && _renderers[i].sharedMaterial.HasProperty(_colorProperty))
                {
                    if (_renderers[i].sharedMaterial.GetColor(_colorProperty) == _bkpMaterials[i].GetColor(_colorProperty))
                    {
                        _renderers[i].sharedMaterial = _bkpMaterials[i];
                    }
                }
            }
        }
        #endregion

        #region Alpha
        void UpdateAlpha()
        {
            if (_alpha == null || _alpha.From == _alpha.To) return;

            Color newColor = new Color();
            float alpha = Mathf.Clamp01(CalculateLerp(_alpha.From, _alpha.To));

            for (int i = 0; i < _renderers.Length; i++)
            {
                if (_renderers[i] == null) continue;

                Tween[] tweens = _renderers[i].GetComponents<Tween>();
                for (int j = 0; j < tweens.Length; j++)
                {
                    if (tweens[j]._alpha.From != tweens[j]._alpha.To && (tweens[j].IsPlaying && !tweens[j].IsPaused))
                        continue;
                }

                TextMesh tm = _renderers[i].GetComponent<TextMesh>();

                if (tm)
                {
                    newColor = tm.color;
                    newColor.a = alpha;
                    tm.color = newColor;
                }
                else if (_renderers[i] is SpriteRenderer)
                {
                    SpriteRenderer render = (SpriteRenderer)_renderers[i];
                    newColor = render.color;
                    newColor.a = alpha;
                    render.color = newColor;
                }

                //TODO: rever uso de shared materials
                else if (_renderers[i].material == null)
                    continue;
                else if (_renderers[i].material.HasProperty(_colorProperty))
                {
                    newColor = _renderers[i].material.GetColor(_colorProperty);
                    newColor.a = alpha;
                    _renderers[i].material.SetColor(_colorProperty, newColor);
                }
            }

            if (_images == null)
                return;

            for (int j = 0; j < _images.Length; j++)
            {
                if (_images[j] == null)
                    continue;

                newColor = _images[j].color;
                newColor.a = alpha;
                _images[j].color = newColor;
            }
        }
        #endregion

        #region Float
        void UpdateFloat()
        {
            if (_float == null || _float.From == _float.To || (_float.Value == _float.To && ElapsedTime >= Duration)) return;

            _float.Value = CalculateLerp(_float.From, _float.To);
        }
        #endregion

        #region TimedCallbacks
        void CheckTimedCallback()
        {
            if (timedCallbacks == null) return;

            for (int i = 0; i < timedCallbacks.Count; i++)
            {
                if (timedCallbacks[i].Time >= ElapsedTime - Time.deltaTime && timedCallbacks[i].Time <= ElapsedTime)
                {
                    timedCallbacks[i].Action();
                }
            }
        }
        #endregion
        #endregion

        #region Revert
        void ReverseDirection()
        {
            _hasReverted = !_hasReverted;

            _position = new PTTVector3(_position.To, _position.From, _position.IsLocal, _position.IsRelative, _position.IgnoreAxis);
            _rotation = new PTTVector3(_rotation.To, _rotation.From, _rotation.IsLocal, _rotation.IsRelative, _rotation.IgnoreAxis);
            _scale = new PTTVector3(_scale.To, _scale.From, _scale.IsLocal, _scale.IsRelative, _scale.IgnoreAxis);
            _color = new PTTColor(_color.To, _color.From);
            _alpha = new PTTFloat(_alpha.To, _alpha.From);
            _float = new PTTFloat(_float.To, _float.From);

            if (Curve != null && FlipCurveOnReverse && EasingReference == EasingReference.Curve)
                Curve = ReverseCurve(Curve);
        }

        AnimationCurve ReverseCurve(AnimationCurve curve)
        {
            Keyframe[] keys = new Keyframe[curve.keys.Length];
            float totalTime = curve.keys[keys.Length - 1].time;

            for (int i = keys.Length - 1; i >= 0; i--)
            {
                Keyframe key = curve.keys[keys.Length - 1 - i];

                keys[i] = new Keyframe(totalTime - key.time, 1 - key.value, key.inTangent, key.outTangent);
            }

            return new AnimationCurve(keys);
        }

        #endregion
    }

    #region EasingEquations
    public class Ease
    {
        public enum Equation
        {
            Linear,
            OutExpo, InExpo, InOutExpo, OutInExpo,
            OutCirc, InCirc, InOutCirc, OutInCirc,
            //OutQuad, InQuad, InOutQuad, OutInQuad,
            OutSine, InSine, InOutSine, OutInSine,
            //OutCubic, InCubic, InOutCubic, OutInCubic,
            //OutQuartic, InQuartic, InOutQuartic, OutInQuartic, 
            //OutQuintic, InQuintic, InOutQuintic, OutInQuintic,
            OutElastic, InElastic, InOutElastic, OutInElastic,
            OutBounce, InBounce, InOutBounce, OutInBounce,
            OutBack, InBack, InOutBack, OutInBack
        }

        #region Linear
        public static float Linear(float time, float start, float end, float duration)
        {
            return end * time / duration + start;
        }
        #endregion

        #region Expo
        public static float OutExpo(float time, float start, float end, float duration)
        {
            return (time == duration) ? start + end : end * (-Mathf.Pow(2, -10 * time / duration) + 1) + start;
        }

        public static float InExpo(float time, float start, float end, float duration)
        {
            return (time == 0) ? start : end * Mathf.Pow(2, 10 * (time / duration - 1)) + start;
        }

        public static float InOutExpo(float time, float start, float end, float duration)
        {
            if (time == 0)
                return start;

            if (time == duration)
                return start + end;

            if ((time /= duration / 2) < 1)
                return end / 2 * Mathf.Pow(2, 10 * (time - 1)) + start;

            return end / 2 * (-Mathf.Pow(2, -10 * --time) + 2) + start;
        }

        public static float OutInExpo(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return OutExpo(time * 2, start, end / 2, duration);

            return InExpo((time * 2) - duration, start + end / 2, end / 2, duration);
        }
        #endregion

        #region Circular
        public static float OutCirc(float time, float start, float end, float duration)
        {
            return end * Mathf.Sqrt(1 - (time = time / duration - 1) * time) + start;
        }

        public static float InCirc(float time, float start, float end, float duration)
        {
            return -end * (Mathf.Sqrt(1 - (time /= duration) * time) - 1) + start;
        }

        public static float InOutCirc(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return -end / 2 * (Mathf.Sqrt(1 - time * time) - 1) + start;

            return end / 2 * (Mathf.Sqrt(1 - (time -= 2) * time) + 1) + start;
        }

        public static float OutInCirc(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return OutCirc(time * 2, start, end / 2, duration);

            return InCirc((time * 2) - duration, start + end / 2, end / 2, duration);
        }
        #endregion

        #region Quad
        #endregion

        #region Sine
        public static float OutSine(float time, float start, float end, float duration)
        {
            return end * Mathf.Sin(time / duration * (Mathf.PI / 2)) + start;
        }

        public static float InSine(float time, float start, float end, float duration)
        {
            return -end * Mathf.Cos(time / duration * (Mathf.PI / 2)) + end + start;
        }

        public static float InOutSine(float time, float start, float end, float duration)
        {
            if ((time /= duration / 2) < 1)
                return end / 2 * (Mathf.Sin(Mathf.PI * time / 2)) + start;

            return -end / 2 * (Mathf.Cos(Mathf.PI * --time / 2) - 2) + start;
        }

        public static float OutInSine(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return OutSine(time * 2, start, end / 2, duration);

            return InSine((time * 2) - duration, start + end / 2, end / 2, duration);
        }
        #endregion

        #region Cubic
        #endregion

        #region Quartic
        #endregion

        #region Quintic
        #endregion

        #region Elastic
        public static float OutElastic(float time, float start, float end, float duration)
        {
            if ((time /= duration) == 1)
                return start + end;

            float p = duration * 0.3f;
            float s = p / 4;

            return (end * Mathf.Pow(2, -10 * time) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p) + end + start);
        }

        public static float InElastic(float time, float start, float end, float duration)
        {
            if ((time /= duration) == 1)
                return start + end;

            float p = duration * 0.3f;
            float s = p / 4;

            return -(end * Mathf.Pow(2, 10 * (time -= 1)) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p)) + start;
        }

        public static float InOutElastic(float time, float start, float end, float duration)
        {
            if ((time /= duration / 2) == 2)
                return start + end;

            float p = duration * (0.3f * 1.5f);
            float s = p / 4;

            if (time < 1)
                return -0.5f * (end * Mathf.Pow(2, 10 * (time -= 1)) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p)) + start;

            return end * Mathf.Pow(2, -10 * (time -= 1)) * Mathf.Sin((time * duration - s) * (2 * Mathf.PI) / p) * 0.5f + end + start;
        }

        public static float OutInElastic(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return OutElastic(time * 2, start, end / 2, duration);

            return InElastic((time * 2) - duration, start + end / 2, end / 2, duration);
        }
        #endregion

        #region Bounce
        public static float OutBounce(float time, float start, float end, float duration)
        {
            if ((time /= duration) < (1 / 2.75f))
                return end * (7.5625f * time * time) + start;
            else if (time < (2 / 2.75f))
                return end * (7.5625f * (time -= (1.5f / 2.75f)) * time + 0.75f) + start;
            else if (time < (2.5f / 2.75f))
                return end * (7.5625f * (time -= (2.25f / 2.75f)) * time + 0.9375f) + start;
            else
                return end * (7.5625f * (time -= (2.625f / 2.75f)) * time + 0.984375f) + start;
        }

        public static float InBounce(float time, float start, float end, float duration)
        {
            return end - OutBounce(duration - time, 0, end, duration) + start;
        }

        public static float InOutBounce(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return InBounce(time * 2, 0, end, duration) * 0.5f + start;

            return OutBounce(time * 2 - duration, 0, end, duration) * 0.5f + end * 0.5f + start;
        }

        public static float OutInBounce(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return OutBounce(time * 2, start, end / 2, duration);

            return InBounce((time * 2) - duration, start + end / 2, end / 2, duration);
        }
        #endregion

        #region Back
        public static float OutBack(float time, float start, float end, float duration)
        {
            return end * ((time = time / duration - 1) * time * ((1.70158f + 1) * time + 1.70158f) + 1) + start;
        }

        public static float InBack(float time, float start, float end, float duration)
        {
            return end * (time /= duration) * time * ((1.70158f + 1) * time - 1.70158f) + start;
        }

        public static float InOutBack(float time, float start, float end, float duration)
        {
            float s = 1.70158f;
            if ((time /= duration / 2) < 1)
                return end / 2 * (time * time * (((s *= (1.525f)) + 1) * time - s)) + start;

            return end / 2 * ((time -= 2) * time * (((s *= (1.525f)) + 1) * time + s) + 2) + start;
        }

        public static float OutInBack(float time, float start, float end, float duration)
        {
            if (time < duration / 2)
                return OutBack(time * 2, start, end / 2, duration);

            return InBack((time * 2) - duration, start + end / 2, end / 2, duration);
        }
        #endregion
    }
    #endregion

}

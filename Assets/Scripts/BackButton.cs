using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() =>
        {
            if (SceneManager.GetActiveScene().name == "ChallengeSelection")
                Application.Quit();
            else
                SceneManager.LoadScene("ChallengeSelection");
        });
    }
}

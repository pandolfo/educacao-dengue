using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterData
{
    public string Name;
    public Sprite Sprite;
    [TextArea] public string Description;
}

[CreateAssetMenu(menuName = "Data/Characters")]
public class CharactersData : ScriptableObject
{
    [SerializeField] private List<CharacterData> _characters;
    public List<CharacterData> Characters => _characters;

    public CharacterData GetCharacterData(int index)
    {
        return _characters[index];
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintButton : MonoBehaviour
{
    private GameManager _managerInstance;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Start()
    {
        Button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        if (_managerInstance == null)
            _managerInstance = FindObjectOfType<GameManager>();

        _managerInstance.ShowCharacterPopup(ChallengeSelectionManager.CurrentChallengeData?.Hint);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PDFButton : MonoBehaviour
{
    [SerializeField] private string _link;
    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Start()
    {
        Button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        if (!string.IsNullOrEmpty(_link))
            Application.OpenURL(_link);
    }
}

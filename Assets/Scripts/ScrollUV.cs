using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollUV : MonoBehaviour
{
    [SerializeField] private Vector2 _speed;
    [SerializeField] private Image _image;

    private Material _material;

    private void FixedUpdate()
    {
        if (_material == null)
        {
            _material = new Material(_image.material);
            _image.material = _material;
        }

        _material.mainTextureOffset += _speed * Time.deltaTime;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerData
{
    public const string NameKey = "PlayerName";
    public const string AvatarKey = "Avatar";
    public const string StarsKey = "StarsMinigame";
    public const string UnlockedGamesKey = "UnlockedGames";

    public static string Name
    {
        get => GetStringKey(NameKey);
        set => SetStringKey(NameKey, value);
    }

    public static int AvatarIndex
    {
        get => GetIntKey(AvatarKey);
        set => SetIntKey(AvatarKey, value);
    }

    public static int UnlockedGames
    {
        get => GetIntKey(UnlockedGamesKey);
        set => SetIntKey(UnlockedGamesKey, value);
    }

    public static int GetMinigameStars(int minigameId)
    {
        return GetIntKey(GetMinigameKey(minigameId));
    }

    public static void SetMinigameStars(int minigameId, int stars)
    {
        SetIntKey(GetMinigameKey(minigameId), stars);
    }

    public static int GetTotalStars()
    {
        int totalStars = 0;

        for (int i = 1; PlayerPrefs.HasKey(GetMinigameKey(i)); i++)
            totalStars += GetIntKey(GetMinigameKey(i));

        return totalStars;
    }

    private static string GetStringKey(string key)
    {
        if (!PlayerPrefs.HasKey(key))
            PlayerPrefs.SetString(key, string.Empty);

        return PlayerPrefs.GetString(key);
    }

    private static void SetStringKey(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    private static int GetIntKey(string key)
    {
        if (!PlayerPrefs.HasKey(key))
            PlayerPrefs.SetInt(key, 0);

        return PlayerPrefs.GetInt(key);
    }

    private static void SetIntKey(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }

    private static string GetMinigameKey(int index)
    {
        return $"{StarsKey}{index}";
    }
}

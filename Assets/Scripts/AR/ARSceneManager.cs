using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class ARSceneManager : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Camera _arCamera;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private ARPlaneManager _planeManager;
    [SerializeField] private ARSession _arSession;

    [Header("UI")]
    [SerializeField] private GameObject _header;
    [SerializeField] private Button _photoButton;
    [SerializeField] private Button _moveButton;
    [SerializeField] private ShareButton _shareButton;
    [SerializeField] private UnsuportedWebcamTexture _webcamTexture;

    private GameObject _spawnedObject;
    private Vector2 _touchPosition;

    private Texture2D _screenshotTexture;
    private string _screenshotPath;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private ARRaycastManager _arRaycastManager;
    public ARRaycastManager ARRaycastManager
    {
        get
        {
            if (_arRaycastManager == null)
                _arRaycastManager = GetComponent<ARRaycastManager>();

            return _arRaycastManager;
        }
    }

    private IEnumerator Start()
    {
        if (ARSession.state == ARSessionState.None ||
            ARSession.state == ARSessionState.CheckingAvailability)
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            // Start some fallback experience for unsupported devices
            _header.SetActive(false);
            _camera.gameObject.SetActive(true);
            _moveButton.gameObject.SetActive(false);

            Debug.LogWarning("AR not supported");

            var obj = Instantiate(_prefab, _camera.transform);
            obj.transform.localPosition = new Vector3(0f, -0.4f, 1);
            obj.transform.localEulerAngles = new Vector3(0, -25, 0);

            _webcamTexture.gameObject.SetActive(true);
        }
        else
        {
            // Start the AR session
            _arSession.enabled = true;
        }

        _shareButton.OnShareCompleted += ShareCompletedCallback;
    }

    bool TryToGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    private void Update()
    {
        if (!_arSession.enabled)
            return;

        if (_spawnedObject != null)
            return;

        if (!TryToGetTouchPosition(out Vector2 touchPosition))
            return;

        if (ARRaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = hits[0].pose;

            //if (_spawnedObject == null)
            //{
                _spawnedObject = Instantiate(_prefab, hitPose.position, hitPose.rotation);
                SetPlanesActive(false);

                _moveButton.interactable = true;
            //}
            //else
            //    _spawnedObject.transform.position = hitPose.position;
        }
    }

    private void SetPlanesActive(bool active)
    {
        _planeManager.SetTrackablesActive(active);
    }

    public void OnPhotoButtonClicked()
    {
        StartCoroutine(TakePhoto());
    }

    public void OnMoveButtonClicled()
    {
        Destroy(_spawnedObject);
        SetPlanesActive(true);
        _moveButton.interactable = false;
    }

    void ShareCompletedCallback()
    {
        _shareButton.gameObject.SetActive(false);
        _photoButton.gameObject.SetActive(true);
    }

    IEnumerator TakePhoto()
    {
        yield return CaptureScreenShot();

        _shareButton.gameObject.SetActive(true);
        _shareButton.SetScreenshotPath(_screenshotPath);

        _photoButton.gameObject.SetActive(false);
    }

    IEnumerator CaptureScreenShot()
    {
        _header.gameObject.SetActive(false);
        SetPlanesActive(false);

        yield return new WaitForEndOfFrame();
        System.DateTime time = System.DateTime.Now;
        string path = $"{Application.persistentDataPath}/{Application.productName}-{time.Day}-{time.Month}-{time.Year}_{time.Hour}-{time.Minute}.png";

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        byte[] imageBytes = screenImage.EncodeToPNG();

        System.IO.File.WriteAllBytes(path, imageBytes);

        _screenshotTexture = screenImage;
        _screenshotPath = path;

        SetPlanesActive(true);
        _header.gameObject.SetActive(true);
    }
}

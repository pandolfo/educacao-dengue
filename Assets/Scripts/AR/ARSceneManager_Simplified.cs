using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARSceneManager_Simplified : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private GameObject _prefab;

    [Header("UI")]
    [SerializeField] private Button _photoButton;
    [SerializeField] private ShareButton _shareButton;
    [SerializeField] private UnsuportedWebcamTexture _webcamTexture;

    private Texture2D _screenshotTexture;
    private string _screenshotPath;

    private void Start()
    {
        _camera.gameObject.SetActive(true);

        var obj = Instantiate(_prefab, _camera.transform);
        obj.transform.localPosition = new Vector3(0f, -0.4f, 1);
        obj.transform.localEulerAngles = new Vector3(0, -25, 0);

        _webcamTexture.gameObject.SetActive(true);

        _shareButton.OnShareCompleted += ShareCompletedCallback;
    }

    public void OnPhotoButtonClicked()
    {
        StartCoroutine(TakePhoto());
    }

    void ShareCompletedCallback()
    {
        _shareButton.gameObject.SetActive(false);
        _photoButton.gameObject.SetActive(true);
    }

    IEnumerator TakePhoto()
    {
        yield return CaptureScreenShot();

        _shareButton.gameObject.SetActive(true);
        _shareButton.SetScreenshotPath(_screenshotPath);

        _photoButton.gameObject.SetActive(false);
    }

    IEnumerator CaptureScreenShot()
    {
        yield return new WaitForEndOfFrame();
        System.DateTime time = System.DateTime.Now;
        string path = $"{Application.persistentDataPath}/{Application.productName}-{time.Day}-{time.Month}-{time.Year}_{time.Hour}-{time.Minute}.png";

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        byte[] imageBytes = screenImage.EncodeToPNG();

        System.IO.File.WriteAllBytes(path, imageBytes);

        _screenshotTexture = screenImage;
        _screenshotPath = path;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnsuportedWebcamTexture : MonoBehaviour
{
    [SerializeField] private RawImage _image;
    [SerializeField] private AspectRatioFitter _fitter;

    private void Awake()
    {
        WebCamTexture tex = new WebCamTexture();
        tex.Play();

        _image.texture = tex;

#if PLATFORM_ANDROID
        _image.transform.rotation = Quaternion.AngleAxis(-tex.videoRotationAngle, Vector3.forward);
#endif

        var aspect = (float)tex.width / (float)tex.height;
        _fitter.aspectRatio = aspect;
    }
}

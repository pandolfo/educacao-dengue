﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineManager : MonoBehaviour
{
    private static CoroutineManager _instance;
    public static CoroutineManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CoroutineManager>();
            if (_instance == null)
                _instance = new GameObject("Coroutine Manager").AddComponent<CoroutineManager>();

            return _instance;
        }
    }

    public static Coroutine NewCoroutine(IEnumerator coroutine)
    {
        return Instance.StartCoroutine(coroutine);
    }

    public static Coroutine WaitForSeconds(float seconds, System.Action callback)
    {
        return Instance.StartCoroutine(WaitForSecondsCoroutine(seconds, callback));
    }

    private static IEnumerator WaitForSecondsCoroutine(float seconds, System.Action callback)
    {
        yield return new WaitForSeconds(seconds);

        callback();
    }

    public static Coroutine WaitForEndOfFrame(System.Action callback)
    {
        return Instance.StartCoroutine(WaitForEndOfFrameCoroutine(callback));
    }

    private static IEnumerator WaitForEndOfFrameCoroutine(System.Action callback)
    {
        yield return new WaitForEndOfFrame();

        callback();
    }

    public static void Stop(IEnumerator coroutine)
    {
        Instance.StopCoroutine(coroutine);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ChallengeData
{
    public string Name;
    public int Id;
    public Color Color;
    public string SceneName;
    public bool IsCertification;
    [TextArea] public string Hint;
}

public class ChallengeSelectionManager : MonoBehaviour
{
    public static ChallengeData CurrentChallengeData;
    public static bool ImmediatlyOpenNextChallenge;

    [SerializeField] private ChallengeSelectionButton _buttonPrefab1;
    [SerializeField] private ChallengeSelectionButton _buttonPrefab2;
    [SerializeField] private GameObject _lockPrefab;
    [SerializeField] private RectTransform _buttonsContainer;
    [SerializeField] private Text _challengeCountText;
    [SerializeField] private Text _playerNameText;
    [SerializeField] private Image _playerAvatarImage;

    //[SerializeField] private List<ChallengeData> _challengesData;
    [SerializeField] private ChallengesData _challengesData;
    [SerializeField] private CharactersData _charactersData;

    private static ChallengeSelectionManager _instance;
    public static ChallengeSelectionManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ChallengeSelectionManager>();

            return _instance;
        }
    }

    private void Start()
    {
        AudioManager.PlayMusic(AudioManager.MusicList.MainMenu);

        if (ImmediatlyOpenNextChallenge)
        {
            ImmediatlyOpenNextChallenge = false;
            int nextChallengeIndex = CurrentChallengeData.Id + 1;
            if (nextChallengeIndex <= _challengesData.ChallengesCount)
                OnButtonClicked(_challengesData.Challenges[nextChallengeIndex - 1]);
            else
                OnButtonClicked(_challengesData.FixedChallenges.Find(c => c.Name == "Certificado"));
            return;
        }

        if (PlayerData.UnlockedGames == 0)
            PlayerData.UnlockedGames = 1;

        List<ChallengeData> challenges = new List<ChallengeData>();
        foreach (var item in _challengesData.Challenges)
        {
            if (item.Id <= PlayerData.UnlockedGames)
                challenges.Add(item);
        }
        challenges.AddRange(_challengesData.FixedChallenges);

        for (int i = 0; i < challenges.Count; i++)
        {
            ChallengeData item = challenges[i];
            InstantiateButton(item, i);
        }

        Instantiate(_lockPrefab, _buttonsContainer);

        _challengeCountText.text = $"{ challenges.Count} / { _challengesData.TotalChallenges.Count}";

        _playerNameText.text = PlayerData.Name;

        _playerAvatarImage.sprite = _charactersData.GetCharacterData(PlayerData.AvatarIndex).Sprite;
    }

    private void InstantiateButton(ChallengeData data, int index)
    {
        int btId = index % 2;
        ChallengeSelectionButton button;
        if (btId == 0)
            button = Instantiate(_buttonPrefab1, _buttonsContainer);
        else
            button = Instantiate(_buttonPrefab2, _buttonsContainer);

        button.Init(data);
    }

    public void OnButtonClicked(ChallengeData data)
    {
        CurrentChallengeData = data;
        LoadScene(data.SceneName);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}

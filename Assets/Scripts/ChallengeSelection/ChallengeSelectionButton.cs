using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ChallengeSelectionButton : MonoBehaviour
{
    [SerializeField] private Text _titleText;
    [SerializeField] private Text _numberText;
    [SerializeField] private Image _circleImage;
    [SerializeField] private List<GameObject> _stars;

    private ChallengeData _data;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    public void Init(ChallengeData data)
    {
        _data = data;

        _titleText.text = data.Name;
        _numberText.text = data.Id.ToString();
        _circleImage.color = data.Color;

        Button.onClick.RemoveListener(OnButtonClicked);
        Button.onClick.AddListener(OnButtonClicked);

        for (int i = 0; i < _stars.Count; i++)
            _stars[i].gameObject.SetActive(i < PlayerData.GetMinigameStars(data.Id));
    }

    private void OnButtonClicked()
    {
        ChallengeSelectionManager.Instance.OnButtonClicked(_data);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneManager : MonoBehaviour
{
    private static LoadingSceneManager _instance;
    public static LoadingSceneManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LoadingSceneManager>();
            return _instance;
        }            
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(4f);

        var async = SceneManager.LoadSceneAsync("OnboardingScene");

        while (!async.isDone)
            yield return null;
    }
}

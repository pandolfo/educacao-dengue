using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPopup : Popup
{
    [SerializeField] private List<Image> _stars;

    public void SetStars(int numberOfStars)
    {
        for (int i = 0; i < _stars.Count; i++)
            _stars[i].enabled = i < numberOfStars;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    [SerializeField] protected Text _titleText;
    [SerializeField] protected Text _bodyText;

    public virtual void Open()
    {
        gameObject.SetActive(true);
    }

    public virtual void Close()
    {
        gameObject.SetActive(false);
    }

    public void SetTitle(string title)
    {
        if (_titleText == null)
            return;

        _titleText.text = title;
    }

    public void SetText(string text)
    {
        if (_bodyText == null)
            return;

        _bodyText.text = text;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPopup : Popup
{
    [SerializeField] private Image _characterImage;
    [SerializeField] private List<Sprite> _characterSprites;

    public override void Open()
    {
        Sprite character = _characterSprites[Random.Range(0, _characterSprites.Count)];
        _characterImage.sprite = character;

        base.Open();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Events;
using UnityEngine.UI;

using ZXing;
using ZXing.QrCode;

[System.Serializable]
public class QREvent : UnityEvent { }

public class QRReader : MonoBehaviour
{
    [SerializeField] private string _authenticationKey;
    [SerializeField] private Text _text;
    [SerializeField] private RawImage _camImage;
    [SerializeField] private AspectRatioFitter _aspectRatioFitter;
    [SerializeField] private GameObject _loading;

    public QREvent OnAuthenticated;
        
    private bool _isInitializing;
    private bool _hasInitialized;

    private WebCamTexture camTexture;

    public bool IsAuthenticated
    {
        get
        {
            return PlayerPrefs.GetInt("Authentication") == 1;
        }
        private set
        {
            PlayerPrefs.SetInt("Authentication", value == true ? 1 : 0);
        }
    }

    /*void Start()
    {
        Init();            
    }*/

    private void LateUpdate()
    {
        if (_isInitializing)
            return;

        if (!_hasInitialized)
        {
            Init();
            return;
        }

        if (WebCamTexture.devices.Length <= 0)
            return;

        IBarcodeReader barcodeReader = new BarcodeReader();

        var result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
        if (result != null)
        {
            if (result.Text == _authenticationKey)
            {
                Debug.LogWarning("Authenticated!");

                _text.text = "Autenticado!";
                IsAuthenticated = true;

                //StartCoroutine(WaitAuth());
                OnAuthenticated.Invoke();
                enabled = false;
            }
        }
    }

    private void Init()
    {
        if (IsAuthenticated || Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (Application.platform == RuntimePlatform.WindowsPlayer)
                IsAuthenticated = true;

            OnAuthenticated.Invoke();
            enabled = false;
            //gameObject.SetActive(false);
            return;
        }

        /*if (WebCamTexture.devices.Length <= 0 && !Application.isEditor)
            return;*/

        _isInitializing = true;

#if PLATFORM_ANDROID && !UNITY_EDITOR
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            PermissionCallbacks callbacks = new PermissionCallbacks();
            callbacks.PermissionGranted += CameraAccessGranted;
            callbacks.PermissionDenied += CameraAccessDenied;
            callbacks.PermissionDeniedAndDontAskAgain += CameraAccessDenied;

            Permission.RequestUserPermission(Permission.Camera, callbacks);

            return;
        }
#endif

        StartCoroutine(PermissionGranted());
    }

    private void CameraAccessGranted(string s)
    {
        StartCoroutine(PermissionGranted());
    }

    private void CameraAccessDenied(string s)
    {
        Application.Quit();
    }

    IEnumerator PermissionGranted()
    {
        yield return new WaitForSeconds(0.1f);

        camTexture = new WebCamTexture();
        if (camTexture != null)
        {
            camTexture.Play();
            _camImage.texture = camTexture;

            var aspect = (float)camTexture.width / (float)camTexture.height;
            _aspectRatioFitter.aspectRatio = aspect;

#if PLATFORM_ANDROID
            _camImage.transform.rotation = Quaternion.AngleAxis(-camTexture.videoRotationAngle, Vector3.forward);            
#endif

            _hasInitialized = true;
            _isInitializing = false;

            _loading.SetActive(false);
        }
    }

    IEnumerator WaitAuth()
    {
        yield return new WaitForSeconds(1);

        OnAuthenticated.Invoke();
        //gameObject.SetActive(false);
        enabled = false;
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class CyclesPieceData
{
    public int Id;
    public Sprite Sprite;
    public string Info;
}

public class CyclesGameManager : GameManager
{
    [SerializeField] private List<CyclesPieceData> _piecesData;
    [SerializeField] private CyclesPiece _piecePrefab;
    [SerializeField] private RectTransform _piecesContainer;
    [SerializeField] private Text _actionText;
    [SerializeField] private Button _confirmButton;

    private List<CyclesPiece> _pieces = new List<CyclesPiece>();
    [SerializeField] private List<CyclesSlot> _slots = new List<CyclesSlot>();

    private CyclesPiece _selectedPiece;
    private CyclesSlot _selectedSlot;

    private static CyclesGameManager _instance;
    public static CyclesGameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CyclesGameManager>();

            return _instance;
        }
    }

    private void Start()
    {
        InitGame();
        AudioManager.PlayMusic(AudioManager.MusicList.Cycles);
    }

    private void InitGame()
    {
        for (int i = 0; i < _slots.Count; i++)
            _slots[i].Init(i);

        CreatePieces();

        _confirmButton.onClick.AddListener(OnConfirmButtonClicked);
    }

    private void CreatePieces()
    {
        List<CyclesPieceData> data = new List<CyclesPieceData>(_piecesData);

        for (int i = 0; i < _piecesData.Count; i++)
        {
            int dataIndex = Random.Range(0, data.Count);
            var piece = Instantiate(_piecePrefab, _piecesContainer);
            piece.Init(data[dataIndex]);
            _pieces.Add(piece);

            data.RemoveAt(dataIndex);
        }
    }

    public void PieceSelected(CyclesPiece piece)
    {
        CyclesSlot slot = _slots.Find(s => s.CurrentPiece == piece);

        if (slot != null)
        {
            slot.CurrentPiece = null;
            piece.transform.SetParent(_piecesContainer);

            _actionText.text = "Selecione uma pe�a para posicionar";
        }
        else
        {
            if (_selectedPiece == null)
            {
                _selectedPiece = piece;

                foreach (var item in _pieces)
                    item.Button.interactable = item == _selectedPiece;

                _actionText.text = "Escolha um espa�o no ciclo";
            }
            else
            {
                _selectedPiece = null;

                foreach (var item in _pieces)
                    item.Button.interactable = true;

                _actionText.text = "Selecione uma pe�a para posicionar";
            }
        }

        EnableConfirmButton();
    }

    public void SlotSelected(CyclesSlot slot)
    {
        if (_selectedPiece == null)
            return;

        _selectedSlot = slot;

        slot.CurrentPiece = _selectedPiece;

        _selectedPiece.transform.SetParent(slot.transform);
        _selectedPiece.transform.localPosition = Vector3.zero;

        foreach (var item in _pieces)
            item.Button.interactable = true;

        _selectedPiece = null;

        _actionText.text = "Selecione uma pe�a para posicionar";

        EnableConfirmButton();
    }

    private void EnableConfirmButton()
    {
        foreach (var slot in _slots)
        {
            if (slot.CurrentPiece == null)
            {
                _confirmButton.gameObject.SetActive(false);
                return;
            }
        }

        _confirmButton.gameObject.SetActive(true);
    }

    private void CheckAnswer()
    {
        int startingIndex = _slots.IndexOf(_slots.Find(s => s.CurrentPiece.Id == 0));

        for (int i = 0; i < _slots.Count; i++)
        {
            int index = (i + startingIndex) % _slots.Count;

            if (_slots[index].CurrentPiece.Id != i)
            {
                ProcessError();
                return;
            }
        }

        Victory();
    }

    private void ProcessError()
    {
        Error();

        if (CheckDefeat())
        {
            Defeat();
            return;
        }

        foreach (var slot in _slots)
        {
            slot.CurrentPiece.transform.SetParent(_piecesContainer);
            slot.CurrentPiece = null;
        }

        EnableConfirmButton();
    }

    private void OnConfirmButtonClicked()
    {
        CheckAnswer();
    }
}

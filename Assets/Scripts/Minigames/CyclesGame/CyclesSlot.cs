using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CyclesSlot : MonoBehaviour
{
    private int _id;
    public int Id => _id;

    public CyclesPiece CurrentPiece { get; set; }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    public void Init(int id)
    {
        _id = id;

        Button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        CyclesGameManager.Instance.SlotSelected(this);
    }
}

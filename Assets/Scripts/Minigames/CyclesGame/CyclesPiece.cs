using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CyclesPiece : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private Button _infoButton;

    private int _id;
    public int Id => _id;

    private string _info;
    public string Info => _info;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    public void Init(CyclesPieceData data)
    {
        _image.sprite = data.Sprite;
        _id = data.Id;
        _info = data.Info;

        Button.onClick.RemoveListener(OnButtonClicked);
        Button.onClick.AddListener(OnButtonClicked);
    }

    public void OnButtonClicked()
    {
        CyclesGameManager.Instance.PieceSelected(this);
    }
}

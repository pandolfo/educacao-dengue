using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WordHuntingManager : GameManager
{
    
    private static WordHuntingManager _instance;
    public static WordHuntingManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<WordHuntingManager>();
            return _instance;
        }
    }

    [Header("Event Systems")]
    [SerializeField] private Camera _camera;
    [SerializeField] private GraphicRaycaster _graphicRaycaster;
    [SerializeField] private EventSystem _eventSystem;

    [Header("References")]
    [SerializeField] private RectTransform _boardContainer;
    [SerializeField] private WordHuntingAnswersUI _answersUI;
    [SerializeField] private WordHuntingTSVReader _tsvReader;
    [SerializeField] private Text _titleText;

    [Header("Slots Config")]
    [SerializeField] private WordHuntingPiece _piecePrefab;
    [SerializeField] private Color _neutralSlotColor = new Color(1, 1, 1, 0);
    [SerializeField] private Color _selectedSlotColor = Color.yellow;
    [SerializeField] private Color _neutralSlotFontColor = Color.white;
    [SerializeField] private Color _selectedSlotFontColor = Color.black;
    [SerializeField] private Color _slotBGColor1 = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    [SerializeField] private Color _slotBGColor2 = new Color(0.25f, 0.25f, 0.25f, 0.5f);

    private WordHuntingData _data;

    private List<List<string>> _board = new List<List<string>>() 
    {
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" },
        new List<string>() { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a" }
    };

    private List<string> _words = new List<string>() { "aaaaa", "aaa", "aaaaaa", "aaaaaaa" };

    private List<WordHuntingPiece> _pieces = new List<WordHuntingPiece>();
    private WordHuntingPiece _firstPiece = null;
    private WordHuntingPiece _currentPiece = null;

    private Vector3 _dragStartPoint;
    private Vector3 _dragEndPoint;

    private void Awake()
    {
        _data = _tsvReader.GetData();

        _board = _data.Board;

        _words.Clear();

        foreach (var answer in _data.Answers)
        {
            _words.Add(answer.Item1.ToLower());
        }

        InitializeBoard();

        _answersUI.Init(_data.Answers);

        AudioManager.PlayMusic(AudioManager.MusicList.WordHunting);
    }

    private void Update()
    {
        if (_firstPiece != null && _currentPiece != null)
        {
            foreach (var p in _pieces)
                p.SetColor(p.IsCorrect ? _selectedSlotColor : _neutralSlotColor, p.IsCorrect ? _selectedSlotFontColor : _neutralSlotFontColor);

            _dragEndPoint = _currentPiece.RectTransform.position;
            var pieces = LineCast(_dragStartPoint, _dragEndPoint);
        }
    }

    private void InitializeBoard()
    {
        int index = 0;

        foreach (var line in _board)
        {
            foreach (var row in line)
            {
                var p = Instantiate(_piecePrefab, _boardContainer);
                p.Value = row;
                p.SetColor(_neutralSlotColor, _neutralSlotFontColor);
                _pieces.Add(p);

                p.SetBGColor(index % 2 == 0 ? _slotBGColor1 : _slotBGColor2);
                index++;
            }
        }

        _titleText.text = _data.Title;
    }

    public void SetDragStartPoint(WordHuntingPiece piece)
    {
        _firstPiece = piece;
        _dragStartPoint = piece.RectTransform.position;
    }

    public void SetDragCurrentPoint(WordHuntingPiece piece)
    {
        _currentPiece = piece;
    }

    public void SetDragEndPoint(WordHuntingPiece piece)
    {
        _firstPiece = null;
        _currentPiece = null;

        var pieces = LineCast(_dragStartPoint, _dragEndPoint);
        bool isCorrect = CheckAnswer(pieces);

        foreach (var p in pieces)
        {
            if (!p.IsCorrect)
                p.IsCorrect = isCorrect;
            p.SetColor(p.IsCorrect ? _selectedSlotColor : _neutralSlotColor, p.IsCorrect ? _selectedSlotFontColor : _neutralSlotFontColor);
        }
    }

    private List<WordHuntingPiece> LineCast(Vector3 startingPoint, Vector3 endingPoint)
    {
        List<WordHuntingPiece> pieces = new List<WordHuntingPiece>();

        for (float i = 0; i < 1; i += 0.05f)
        {
            Vector3 position = Vector3.Lerp(startingPoint, endingPoint, i);

            var pointerData = new PointerEventData(_eventSystem);
            pointerData.position = position;

            List<RaycastResult> results = new List<RaycastResult>();

            _graphicRaycaster.Raycast(pointerData, results);

            foreach (RaycastResult result in results)
            {
                var hit = result.gameObject.GetComponent<WordHuntingPiece>();

                if (hit == null) 
                    continue;

                if (!pieces.Contains(hit))
                    pieces.Add(hit);

                hit.SetColor(_selectedSlotColor, _selectedSlotFontColor);
            }
        }

        return pieces;
    }

    private bool CheckAnswer(List<WordHuntingPiece> pieces)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var piece in pieces)
            sb.Append(piece.Value);
        string answer = sb.ToString();

        bool isCorrect = _words.Contains(answer.ToLower());

        if (isCorrect)
        {
            Correct();

            _words.Remove(answer.ToLower());
            _answersUI.SetAnswerFound(answer, true);
        }
        else
        {
            Error();

            if (CheckDefeat())
                Defeat();
        }

        if (_words.Count <= 0)
            Victory();

        return isCorrect;
    }

    public void ShowHint()
    {
        ShowCharacterPopup(_data.Hint);
    }

    public void ShowInfo(string info)
    {
        ShowCharacterPopup(info);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WordHuntingPiece : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler
{
    [SerializeField] private string _value;
    [SerializeField] private Text _text;
    [SerializeField] private Image _image;
    [SerializeField] private Image _bgImage;

    public string Value
    {
        get => _value;
        set
        {
            _value = value;
            _text.text = _value;
        }
    }

    private bool _isCorrect;
    public bool IsCorrect
    {
        get => _isCorrect;
        set => _isCorrect = value;
    }

    private RectTransform _rectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();
            return _rectTransform;
        }
    }

    public void SetColor(Color bgColor, Color textColor)
    {
        SetSelectionColor(bgColor);
        SetTextColor(textColor);
    }

    public void SetSelectionColor(Color color)
    {
        _image.color = color;
    }

    public void SetBGColor(Color color)
    {
        _bgImage.color = color;
    }

    public void SetTextColor(Color color)
    {
        _text.color = color;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        WordHuntingManager.Instance.SetDragStartPoint(this);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        WordHuntingManager.Instance.SetDragEndPoint(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        WordHuntingManager.Instance.SetDragCurrentPoint(this);
    }
}

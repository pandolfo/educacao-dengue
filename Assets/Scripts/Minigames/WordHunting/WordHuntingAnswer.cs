using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordHuntingAnswer : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private Button _infoButton;
    [SerializeField] private RectTransform _foundElements;
    [SerializeField] private RectTransform _notFoundElements;

    private string _infoText;

    private bool _found;
    public bool Found
    {
        get => _found;
        set
        {
            _found = value;

            _foundElements.gameObject.SetActive(_found);
            _notFoundElements.gameObject.SetActive(!_found);
        }
    }

    public void Init(string word, string info)
    {
        _text.text = word;
        _infoText = info;

        Found = false;

        _infoButton.onClick.AddListener(() => 
        {
            WordHuntingManager.Instance.ShowInfo(_infoText);
        });
    }
}

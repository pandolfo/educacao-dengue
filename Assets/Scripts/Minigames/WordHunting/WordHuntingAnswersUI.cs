using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordHuntingAnswersUI : MonoBehaviour
{
    [SerializeField] private Text _wordCountText;
    [SerializeField] private WordHuntingAnswer _answerPrefab;
    [SerializeField] private RectTransform _answersContainer;

    private int _totalWords;
    private Dictionary<string, WordHuntingAnswer> _answersDict = new Dictionary<string, WordHuntingAnswer>();

    private int _wordCount;
    public int WordCount
    {
        get => _wordCount;
        set
        {
            _wordCount = value;
            _wordCountText.text = $"PALAVRAS: {_wordCount} / {_totalWords}";
        }
    }

    public void Init(List<(string, string)> answers)
    {
        foreach (var answer in answers)
        {
            var answerGO = Instantiate(_answerPrefab, _answersContainer);
            answerGO.Init(answer.Item1, answer.Item2);

            _answersDict.Add(answer.Item1.ToLower(), answerGO);
        }

        _totalWords = answers.Count;
        WordCount = 0;
    }

    public void SetAnswerFound(string answer, bool isFound)
    {
        _answersDict[answer.ToLower()].Found = isFound;

        int count = 0;
        foreach (var item in _answersDict)
        {
            if (item.Value.Found)
                count++;
        }

        WordCount = count;
    }
}

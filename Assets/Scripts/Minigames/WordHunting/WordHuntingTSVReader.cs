using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordHuntingData
{
    public List<List<string>> Board;
    public string Hint;
    public List<(string, string)> Answers;
    public string Title;

    public WordHuntingData()
    {
        Board = new List<List<string>>();
        Answers = new List<(string, string)>();
    }
}

public class WordHuntingTSVReader : MonoBehaviour
{
    [SerializeField] private List<TextAsset> _tsvs;
    [SerializeField] private int _size = 10;

    public WordHuntingData GetData()
    {
        var tsv = _tsvs[Random.Range(0, _tsvs.Count)];
        return ReadTSV(tsv);
    }

    private WordHuntingData ReadTSV(TextAsset tsv)
    {
        WordHuntingData data = new WordHuntingData();

        string[] lines = tsv.text.Trim().Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.None);

        for (int i = 0; i < lines.Length; i++)
        {
            if (i < _size)
            {
                string[] columns = lines[i].Split("\t".ToCharArray());

                List<string> letters = new List<string>();
                foreach (var column in columns)
                    letters.Add(column.Trim());

                data.Board.Add(new List<string>(letters));
            }
            else if (i == _size)
            {
                data.Hint = lines[i].Trim();
            }
            else
            {
                string[] answer = lines[i].Split(new string[] { "\t" }, System.StringSplitOptions.None);
                data.Answers.Add((answer[0].Trim(), answer[1].Trim()));
            }
        }
        data.Title = tsv.name.Split('-')[1].Trim();

        return data;
    }
}

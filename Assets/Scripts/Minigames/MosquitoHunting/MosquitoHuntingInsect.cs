using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MosquitoHuntingInsect : MonoBehaviour
{
    [SerializeField] private InsectType _type;
    [SerializeField] private Sprite _normalSprite;
    [SerializeField] private Sprite _deadSprite;
    [SerializeField] private int _sizeMultiplier = -1;

    public InsectType Type => _type;

    private RectTransform _rectTransform;
    public RectTransform RectTransform => _rectTransform;

    private Button _button;
    private Image _image;
    private Vector2 _speed;

    private float _randomizer;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _image = GetComponent<Image>();
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnButtonClicked);

        _randomizer = Random.Range(0, 50f);
    }

    private void FixedUpdate()
    {
        _rectTransform.anchoredPosition += Vector2.one * _speed;
        _rectTransform.anchoredPosition += Vector2.up * Mathf.Sin(Time.time + _randomizer);

        if (!MosquitoHuntingGameManager.Instance.IsInsideArea(this))
            Destroy(gameObject);
    }

    public  void Init(float speed)
    {
        _speed = new Vector2(speed, 0);
        _rectTransform.localScale = new Vector3(Mathf.Sign(speed) * _sizeMultiplier, 1, 1);
    }

    private void OnButtonClicked()
    {
        MosquitoHuntingGameManager.Instance.InsectClicked(this);
    }

    public void SetDeath()
    {
        _image.sprite = _deadSprite;
        _speed = new Vector2(0, -5);
        _button.interactable = false;
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        float t = 0;
        float duration = 0.5f;

        Color color = _image.color;

        while(t < duration)
        {
            t += Time.deltaTime;
            color.a = Mathf.Lerp(1, 0, t / duration);
            _image.color = color;
            yield return new WaitForEndOfFrame();
        }

        Destroy(gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InsectType
{
    None,
    Mosquito,
    Insect
}

public class MosquitoHuntingGameManager : GameManager
{
    [SerializeField] private float _spawnRate;
    [SerializeField] private float _insectsSpeed = 10;
    [SerializeField] private List<MosquitoHuntingInsect> _insectPrefabs;
    [SerializeField] private RectTransform _insectContainer;
    [SerializeField] private Text _pointsText;

    private int _points;
    public int Points
    {
        get => _points;
        set
        {
            _points = value;
            _pointsText.text = $"{_points} / 10";
        }
    }

    private static MosquitoHuntingGameManager _instance;
    public static MosquitoHuntingGameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MosquitoHuntingGameManager>();
            return _instance;
        }
    }

    private void Start()
    {
        StartCoroutine(SpawnRoutine());

        AudioManager.PlayMusic(AudioManager.MusicList.MosquitoHunting);
    }

    private void SpawnInsect()
    {
        float randomSpeed = Mathf.Sign(Random.Range(-1, 1)) * _insectsSpeed;

        float x = randomSpeed > 0 ? _insectContainer.rect.xMin : _insectContainer.rect.xMax;
        float y = Random.Range(_insectContainer.rect.yMin, _insectContainer.rect.yMax);

        int insectIndex = Random.Range(0, _insectPrefabs.Count);

        var insect = Instantiate(_insectPrefabs[insectIndex], _insectContainer);
        insect.RectTransform.anchoredPosition = new Vector2(x, y);
        insect.Init(randomSpeed);
    }

    public void InsectClicked(MosquitoHuntingInsect insect)
    {
        if (insect.Type != InsectType.Mosquito)
            ProcessError();
        else
            ProcessKill();

        insect.SetDeath();
    }

    public bool IsInsideArea(MosquitoHuntingInsect insect)
    {
        return _insectContainer.rect.Contains(insect.RectTransform.anchoredPosition);
    }

    private void ProcessError()
    {
        Error();

        if (CheckDefeat())
        {
            Defeat();
            StopAllCoroutines();
        }
    }

    private void ProcessKill()
    {
        Correct();

        Points++;
        if (Points >= 10)
        {
            Victory();
            StopAllCoroutines();
        }
    }

    IEnumerator SpawnRoutine()
    {
        while (gameObject != null)
        {
            yield return new WaitForSeconds(_spawnRate);

            SpawnInsect();
        }
    }
}

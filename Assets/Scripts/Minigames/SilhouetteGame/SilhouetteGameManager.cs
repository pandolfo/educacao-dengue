using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SilhouetteGameManager : GameManager
{
    private static SilhouetteGameManager _instance;
    public static SilhouetteGameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SilhouetteGameManager>();

            return _instance;
        }
    }

    [SerializeField] private GameObject _step1;
    [SerializeField] private GameObject _step2;
    [SerializeField] private GameObject _mask;
    [SerializeField] private Text _characteristicsFoundText;
    [SerializeField] private Text _characteristicsSelectedText;
    [SerializeField] private Button _confirmButton;

    [HideInInspector][SerializeField] private List<CharacteristicAnswerButton> _answerButtons = new List<CharacteristicAnswerButton>();
    [SerializeField] private List<CharacteristicSelectedAnswerButton> _selectedButtons = new List<CharacteristicSelectedAnswerButton>();
    [SerializeField] private List<CharacteristicToggle> _toggles = new List<CharacteristicToggle>();

    [SerializeField] private List<Color> _colors = new List<Color>();
    [SerializeField] private CharacteristicAnswerButton _buttonPrefab;
    [SerializeField] private Transform _characteristicsContainer;

    private List<string> _characteristics = new List<string>() 
    {
        "1 par de antenas",
        "3 pares de patas",
        "manchas brancas",
        "ferrão",
        "2 pares de asas",
        "corpo arredondado",
        "manchas brancas"
    };
    private List<int> _characteristicsFound = new List<int>();
    private List<int> _wrongCharacteristics = new List<int>()
    {
        3,
        5
    };

    private void Start()
    {
        AudioManager.PlayMusic(AudioManager.MusicList.Characteristics);
    }

    public void CharacteristicFound(int id)
    {
        Correct();

        _characteristicsFound.Add(id);
        _characteristicsFoundText.text = $"Características: {_characteristicsFound.Count} / {_characteristics.Count}";

        if (_characteristicsFound.Count == _characteristics.Count)
            StartCoroutine(WaitToStartStep2());
    }

    public void StartStep2()
    {
        _mask.gameObject.SetActive(false);
        _step1.SetActive(false);
        _step2.SetActive(true);

        for (int i = 0; i < _characteristics.Count; i++)
        {
            //var characteristic = Instantiate(_buttonPrefab, _characteristicsContainer);
            //characteristic.Init(i, _colors[i], _characteristics[i]);
            //_answerButtons.Add(characteristic);

            _toggles[i].Init(i, _colors[i], _characteristics[i]);
        }
    }

    IEnumerator WaitToStartStep2()
    {
        yield return new WaitForSeconds(1f);

        StartStep2();
    }

    public void CharacteristicSelected(int id)
    {
        var button = _selectedButtons.Find(s => s.IsEmpty);

        if (button != null)
            button.SetContent(id, _colors[id], _characteristics[id]);

        EnableConfirmButton();
    }

    public void CharacteristicSelected(int id, bool isOn)
    {
        /*var button = _selectedButtons.Find(s => s.IsEmpty);

        if (button != null)
            button.SetContent(id, _colors[id], _characteristics[id]);

        EnableConfirmButton();*/

        EnableConfirmButton();
    }

    public void CharacteristicDeselected(int id)
    {
        _answerButtons[id].Selected = false;
        _selectedButtons.Find(s => s.Id == id).IsEmpty = true;

        EnableConfirmButton();
    }

    private int CountSelectedCharacteristics()
    {
        /*int count = 0;
        foreach (var item in _answerButtons)
        {
            if (item.Selected)
                count++;
        }

        _characteristicsSelectedText.text = $"Características selecionadas: {count} / {_wrongCharacteristics.Count}";

        return count;*/

        int count = 0;

        foreach (var item in _toggles)
        {
            if (item.IsOn)
                count++;
        }

        foreach (var item in _toggles)
        {
            //if (count >= _wrongCharacteristics.Count)
            if (count >= _characteristics.Count - _wrongCharacteristics.Count)
            {
                if (!item.IsOn)
                    item.Toggle.interactable = false;
            }
            else
                item.Toggle.interactable = true;
        }

        return count;
    }

    private void CheckAnswer()
    {
        bool wrong = false;
        /*foreach (var item in _selectedButtons)
        {
            if (!_wrongCharacteristics.Contains(item.Id))
            {
                wrong = true;
                break;
            }
        }*/

        foreach (var item in _toggles)
        {
            //if (item.IsOn && !_wrongCharacteristics.Contains(item.Id))
            if (item.IsOn && _wrongCharacteristics.Contains(item.Id))
            {
                wrong = true;
                break;
            }
        }

        if (wrong)
        {
            StartCoroutine(WaitToResetGame());
        }
        else
        {
            foreach (var item in _answerButtons)
                item.Button.interactable = false;

            foreach (var item in _selectedButtons)
                item.Button.interactable = false;

            Victory();
        }
    }

    private void EnableConfirmButton()
    {
        int count = CountSelectedCharacteristics();

        //_confirmButton.gameObject.SetActive(count >= _characteristics.Count - _wrongCharacteristics.Count);
        _confirmButton.gameObject.SetActive(count > 0);
    }

    public void OnConfirmButtonClicked()
    {
        CheckAnswer();
    }

    IEnumerator WaitToResetGame()
    {
        /*foreach (var item in _answerButtons)
            item.Button.interactable = false;
        foreach (var item in _selectedButtons)
            item.Button.interactable = false;*/

        foreach (var item in _toggles)
        {
            item.IsOn = false;
            item.Toggle.interactable = false;
        }

        Error();

        yield return new WaitForSeconds(1f);

        /*foreach (var item in _answerButtons)
            item.Selected = false;
        foreach (var item in _selectedButtons)
            item.IsEmpty = true;*/

        EnableConfirmButton();        
        
        if (CheckDefeat())
            Defeat();
        else
        {
            /*foreach (var item in _answerButtons)
                item.Button.interactable = true;
            foreach (var item in _selectedButtons)
                item.Button.interactable = true;*/

            foreach (var item in _toggles)
                item.Toggle.interactable = true;
        }
    }
}

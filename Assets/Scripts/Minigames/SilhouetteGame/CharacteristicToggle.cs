using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class CharacteristicToggle : MonoBehaviour
{
    [SerializeField] private Image _numberFrame;
    [SerializeField] private Text _numberText;
    [SerializeField] private Text _text;

    private int _id;
    public int Id => _id;

    public bool IsOn 
    {
        get => Toggle.isOn;
        set => Toggle.isOn = value;
    }

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();

            return _toggle;
        }
    }

    public void Init(int id, Color color, string text)
    {
        _numberFrame.color = color;
        _numberText.text = (id + 1).ToString();
        _text.text = text;

        _id = id;

        Toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
        Toggle.onValueChanged.AddListener(OnToggleValueChanged);
    }

    private void OnToggleValueChanged(bool value)
    {
        SilhouetteGameManager.Instance.CharacteristicSelected(_id, value);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacteristicSelectedAnswerButton : MonoBehaviour
{
    [SerializeField] private Image _circleImage;
    [SerializeField] private Text _circleText;
    [SerializeField] private Text _text;
    [SerializeField] private GameObject _emptyContents;
    [SerializeField] private GameObject _filledContents;

    private int _id;
    public int Id => _id;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private bool _isEmpty = true;
    public bool IsEmpty
    {
        get => _isEmpty;
        set
        {
            _isEmpty = value;

            _emptyContents.SetActive(_isEmpty);
            _filledContents.SetActive(!_isEmpty);
        }
    }

    public void SetContent(int id, Color color, string text)
    {
        _circleImage.color = color;
        _circleText.text = (id + 1).ToString();
        _text.text = text;

        _id = id;

        IsEmpty = false;

        Button.onClick.RemoveListener(OnButtonClicked);
        Button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        if (IsEmpty)
            return;

        SilhouetteGameManager.Instance.CharacteristicDeselected(_id);
    }
}

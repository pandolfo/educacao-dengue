using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DisableOnPointerEnter : MonoBehaviour, IPointerEnterHandler
{
    private Image _image;
    public Image Image
    {
        get
        {
            if (_image == null)
                _image = GetComponent<Image>();
            return _image;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Image.enabled = false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacteristicAnswerButton : MonoBehaviour
{
    [SerializeField] private GameObject _selectedElements;
    [SerializeField] private GameObject _notSelectedElements;
    [SerializeField] private Image _circleImage;
    [SerializeField] private Text _circleText;
    [SerializeField] private Text _text;

    public int Id { get; set; }
    public Color Color { get; set; }

    private Button _button;
    public Button Button => _button;

    private bool _selected;
    public bool Selected
    {
        get => _selected;
        set
        {
            _selected = value;

            _selectedElements.SetActive(_selected);
            _notSelectedElements.SetActive(!_selected);
        }
    }

    public void Init(int id, Color color, string text)
    {
        _circleImage.color = color;
        _circleText.text = (id + 1).ToString();
        _text.text = text;

        Id = id;
        Color = color;

        Selected = false;

        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        if (Selected)
            return;

        Selected = true;
        SilhouetteGameManager.Instance.CharacteristicSelected(Id);
    }
}

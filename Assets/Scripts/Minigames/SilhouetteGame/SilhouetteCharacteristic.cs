using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilhouetteCharacteristic : MonoBehaviour
{
    [SerializeField] private int _id;

    public void SetActive(bool active)
    {
        if (transform.GetChild(0).gameObject.activeSelf)
            return;

        transform.GetChild(0).gameObject.SetActive(active);
        SilhouetteGameManager.Instance.CharacteristicFound(_id);
    }
}

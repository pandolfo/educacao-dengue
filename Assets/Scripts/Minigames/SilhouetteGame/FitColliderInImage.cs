using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitColliderInImage : MonoBehaviour
{
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();

        var collider = GetComponent<BoxCollider2D>();
        var rectTr = GetComponent<RectTransform>();

        collider.size = rectTr.sizeDelta - Vector2.one * 15;
    }
}

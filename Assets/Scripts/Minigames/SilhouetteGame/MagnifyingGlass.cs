using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MagnifyingGlass : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector2 _startPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        _startPosition = transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //GetComponent<RectTransform>().anchoredPosition = eventData.position;
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = _startPosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Mask"))
        {
            collision.GetComponent<Image>().enabled = false;//.SetActive(false);
        }
        else if (collision.gameObject.CompareTag("Characteristic"))
        {
            collision.GetComponent<SilhouetteCharacteristic>().SetActive(true);
        }
    }
}

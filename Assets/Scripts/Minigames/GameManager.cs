using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] protected List<GameObject> _stars;

    [Header("Popups")]
    [SerializeField] protected VictoryPopup _victoryPopup;
    [SerializeField] protected Popup _defeatPopup;
    [SerializeField] protected CharacterPopup _characterPopup;

    protected int _errors;

    protected virtual void Victory()
    {
        _victoryPopup.Open();
        _victoryPopup.SetStars(_stars.Count - _errors);

        SaveGameStars();
        UnlockNextChallenge();

        AudioManager.PlaySFX(AudioManager.SFXList.Victory);
    }

    protected virtual void Defeat()
    {
        _defeatPopup.Open();

        AudioManager.PlaySFX(AudioManager.SFXList.Defeat);
    }

    protected virtual void Error()
    {
        _errors++;
        for (int i = 0; i < _stars.Count; i++)
            _stars[i].SetActive(_stars.Count - _errors > i);

        AudioManager.PlaySFX(AudioManager.SFXList.Error);
    }

    protected virtual void Correct()
    {
        AudioManager.PlaySFX(AudioManager.SFXList.Correct);
    }

    protected bool CheckDefeat()
    {
        return _errors >= _stars.Count;
    }

    public void ShowCharacterPopup(string text)
    {
        _characterPopup.Open();
        _characterPopup.SetText(text);
    }

    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void NextChallenge()
    {
        ChallengeSelectionManager.ImmediatlyOpenNextChallenge = true;
        SceneManager.LoadScene("ChallengeSelection");
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("ChallengeSelection");
    }

    public void SaveGameStars()
    {
        int gameId = GetCurrentGameId();
        int currentStars = _stars.Count - _errors;

        if (PlayerData.GetMinigameStars(gameId) < currentStars)
            PlayerData.SetMinigameStars(GetCurrentGameId(), currentStars);
    }

    public void UnlockNextChallenge()
    {
        if (GetCurrentGameId() == PlayerData.UnlockedGames)
            PlayerData.UnlockedGames++;
    }

    public int GetCurrentGameId()
    {
        //return SceneManager.GetActiveScene().buildIndex;
        return ChallengeSelectionManager.CurrentChallengeData.Id;
    }
}

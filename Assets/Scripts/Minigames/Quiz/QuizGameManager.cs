using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TextQuizConfigs
{
    [SerializeField] public GameObject TextQuizContainer;
    [SerializeField] public List<QuizAnswerButton> AnswerButtons = new List<QuizAnswerButton>();
    [SerializeField] public Text QuestionText;
}

[System.Serializable]
public class ImageQuizConfigs
{
    public GameObject ImageQuizContainer;
    public List<QuizAnswerButton> AnswerButtons = new List<QuizAnswerButton>();
    public Text QuestionText;
    public Image QuestionImage;
}

public class QuizGameManager : GameManager
{
    [Header("Configs")]
    [SerializeField] private QuizTSVReader _tsvReader;
    [SerializeField] private TextQuizConfigs _textQuizConfigs = new TextQuizConfigs();
    [SerializeField] private ImageQuizConfigs _imageQuizConfigs = new ImageQuizConfigs();
    [SerializeField] private float _gameTimeInMinutes = 5;

    [Header("UI")]
    [SerializeField] private Text _currentQuestionIndexText;
    [SerializeField] private Text _totalQuestionsText;
    [SerializeField] private Text _timeText;

    private List<QuizTextQuestionData> _textQuestions;
    private List<QuizTextQuestionData> _currentTextQuestions = new List<QuizTextQuestionData>();

    private List<QuizImageQuestionData> _imageQuestions;
    private List<QuizImageQuestionData> _currentImageQuestions = new List<QuizImageQuestionData>();

    private QuizQuestionData _currentQuestion;

    private bool _started = false;
    private float _currentTime = 0;

    private int _currentQuestionIndex = 0;
    public int CurrentQuestionIndex
    {
        get => _currentQuestionIndex;
        set
        {
            _currentQuestionIndex = value;
            _currentQuestionIndexText.text = _currentQuestionIndex.ToString();
        }
    }

    private static QuizGameManager _instance;
    public static QuizGameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<QuizGameManager>();
            return _instance;
        }
    }

    private void Start()
    {
        if (ChallengeSelectionManager.CurrentChallengeData != null && ChallengeSelectionManager.CurrentChallengeData.IsCertification)
            InitImage();
        else
            InitText();

        AudioManager.PlayMusic(AudioManager.MusicList.Quiz);
    }

    private void Update()
    {
        if (!_started)
            return;

        _currentTime += Time.deltaTime;
        var timeSpan = System.TimeSpan.FromMinutes((double)_gameTimeInMinutes);
        var elapsedTime = System.TimeSpan.FromSeconds((double)_currentTime);
        var currentTime = timeSpan - elapsedTime;
        _timeText.text = $"{currentTime.Minutes} : {currentTime.Seconds:00}";

        if (currentTime.TotalSeconds <= 0)
        {
            _started = false;
            Defeat();
        }
    }

    private void InitText()
    {
        _started = true;

        _textQuizConfigs.TextQuizContainer.SetActive(true);
        _imageQuizConfigs.ImageQuizContainer.SetActive(false);

        _textQuestions = _tsvReader.ReadTextTSV();
        _currentTextQuestions.Clear();
        
        var availableQuestions = new List<QuizTextQuestionData>(_textQuestions);
        for (int i = 0; i < 10; i++)
        {
            int randomIndex = Random.Range(0, availableQuestions.Count);
            _currentTextQuestions.Add(availableQuestions[randomIndex]);
            availableQuestions.RemoveAt(randomIndex);
        }

        _totalQuestionsText.text = $"/{_currentTextQuestions.Count}";

        InitTextQuestion();
    }

    private void InitTextQuestion()
    {
        var questionData = _currentTextQuestions[Random.Range(0, _currentTextQuestions.Count)];
        _textQuizConfigs.QuestionText.text = questionData.Question;

        var availableAnswers = new List<string>(questionData.Answers);

        for (int i = 0; i < _textQuizConfigs.AnswerButtons.Count; i++)
        {
            int randomIndex = Random.Range(0, availableAnswers.Count);
            var answer = availableAnswers[randomIndex];
            _textQuizConfigs.AnswerButtons[i].Init(questionData.Answers.IndexOf(answer), answer, questionData.AnswersAudioName[questionData.Answers.IndexOf(answer)]);
            availableAnswers.RemoveAt(randomIndex);
        }

        _currentTextQuestions.Remove(questionData);
        CurrentQuestionIndex++;

        _currentQuestion = questionData;

        AudioManager.PlayVoice(_currentQuestion.QuestionAudioName);
    }

    private void InitImage()
    {
        _started = true;

        _textQuizConfigs.TextQuizContainer.SetActive(false);
        _imageQuizConfigs.ImageQuizContainer.SetActive(true);

        _imageQuestions = _tsvReader.ReadImageTSV();
        _currentImageQuestions.Clear();

        var availableQuestions = new List<QuizImageQuestionData>(_imageQuestions);
        for (int i = 0; i < 10; i++)
        {
            int randomIndex = Random.Range(0, availableQuestions.Count);
            _currentImageQuestions.Add(availableQuestions[randomIndex]);
            availableQuestions.RemoveAt(randomIndex);
        }

        _currentImageQuestions.AddRange(_tsvReader.ReadFixedImgTSV());

        _totalQuestionsText.text = $"/{_currentImageQuestions.Count}";

        InitImageQuestion();
    }

    private void InitImageQuestion()
    {
        var questionData = _currentImageQuestions[0];
        _imageQuizConfigs.QuestionText.text = questionData.Question;
        _imageQuizConfigs.QuestionImage.sprite = questionData.Sprite;

        _imageQuizConfigs.AnswerButtons[0].Init(0, "Sim");
        _imageQuizConfigs.AnswerButtons[1].Init(1, "N�o");

        _currentImageQuestions.RemoveAt(0);
        CurrentQuestionIndex++;

        _currentQuestion = questionData;
    }

    private void NextQuestion()
    {
        if (ChallengeSelectionManager.CurrentChallengeData != null && ChallengeSelectionManager.CurrentChallengeData.IsCertification)
        {
            if (_currentImageQuestions.Count <= 0)
                Victory();
            else
                InitImageQuestion();
        }
        else
        {
            if (_currentTextQuestions.Count <= 0)
                Victory();
            else
                InitTextQuestion();
        }
    }

    public void Answer(int id)
    {
        if (id == _currentQuestion.RightAnswer)
        {
            Correct();
        }
        else
        {
            Error();

            if (CheckDefeat())
                Defeat();
        }

        NextQuestion();
    }
}

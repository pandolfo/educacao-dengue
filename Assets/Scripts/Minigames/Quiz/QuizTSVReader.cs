using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizQuestionData
{
    public string Question;
    public int RightAnswer;
    public string QuestionAudioName;
}

public class QuizTextQuestionData : QuizQuestionData
{
    public List<string> Answers;
    public List<string> AnswersAudioName;
    public string CharacterName;
    public bool Checked;
}

public class QuizImageQuestionData : QuizQuestionData
{
    public Sprite Sprite;
}

public class QuizTSVReader : MonoBehaviour
{
    [SerializeField] private TextAsset _textTsv;
    [SerializeField] private TextAsset _imageTsv;
    [SerializeField] private TextAsset _fixedImageTsv;

    public List<QuizTextQuestionData> ReadTextTSV()
    {
        List<QuizTextQuestionData> questions = new List<QuizTextQuestionData>();

        string tsv = _textTsv.text;

        var lines = tsv.Trim().Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.None);

        //for (int i = 1; i < lines.Length; i++)
        //{
        //    QuizTextQuestionData question = new QuizTextQuestionData();

        //    var columns = lines[i].Trim().Split("\t".ToCharArray());

        //    question.Question = columns[0];
        //    question.Answers = new List<string>
        //    {
        //        columns[1],
        //        columns[2],
        //        columns[3],
        //        columns[4],
        //    };
        //    question.RightAnswer = 3;

        //    questions.Add(question);
        //}

        for (int i = 1; i < lines.Length; i++)
        {
            QuizTextQuestionData question = new QuizTextQuestionData();

            var columns = lines[i].Trim().Split("\t".ToCharArray());

            question.Question = columns[1].Trim();
            question.Answers = new List<string>
            {
                columns[2].Trim(),
                columns[3].Trim(),
                columns[4].Trim(),
                columns[5].Trim()
            };
            question.RightAnswer = 3;

            question.CharacterName = columns[6].Trim();

            question.QuestionAudioName = columns[7].Trim();
            question.AnswersAudioName = new List<string>
            {
                columns[8].Trim(),
                columns[9].Trim(),
                columns[10].Trim(),
                columns[11].Trim()
            };

            question.Checked = bool.Parse(columns[12].Trim().ToLower());

            if (question.Checked)
                questions.Add(question);
        }

        return questions;
    }

    public List<QuizImageQuestionData> ReadImageTSV()
    {
        return ReadImgTSV(_imageTsv.text);
    }

    public List<QuizImageQuestionData> ReadFixedImgTSV()
    {
        return ReadImgTSV(_fixedImageTsv.text);
    }

    private List<QuizImageQuestionData> ReadImgTSV(string tsv)
    {
        List<QuizImageQuestionData> questions = new List<QuizImageQuestionData>();

        var lines = tsv.Trim().Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.None);

        for (int i = 1; i < lines.Length; i++)
        {
            QuizImageQuestionData question = new QuizImageQuestionData();

            var columns = lines[i].Trim().Split("\t".ToCharArray());

            question.Question = columns[0];
            question.RightAnswer = columns[2].Trim().ToLower() == "sim" ? 0 : 1;
            question.Sprite = Resources.Load<Sprite>($"Certification/{columns[3].Trim()}");

            questions.Add(question);
        }

        return questions;
    }
}

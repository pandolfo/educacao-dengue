using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizAnswerButton : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private Button _audioButton;

    private int _id;
    private string _audioName;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    public void Init(int id, string text, string audioName = "")
    {
        _id = id;
        _text.text = text;
        _audioName = audioName;

        Button.onClick.RemoveListener(OnButtonClicked);
        Button.onClick.AddListener(OnButtonClicked);

        if (_audioButton != null)
        {
            _audioButton.onClick.RemoveListener(OnAudioButtonClicked);
            _audioButton.onClick.AddListener(OnAudioButtonClicked);
        }
    }

    private void OnButtonClicked()
    {
        QuizGameManager.Instance.Answer(_id);
    }

    private void OnAudioButtonClicked()
    {
        AudioManager.PlayVoice(_audioName);
    }
}
